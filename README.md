
Simple Timeclock frontend

This project contains the frontend of an application that is intended to function as a simple timeclock for fire departments

This project was made with NextJS.

How to Run:

    1. Download and unzip source code.
    2. Run command: npm install
    3. Run command: npm run start

How to Build:

    1. Download and unzip source code.
    2. Run command: npm install
    3. Run command: npm run build




import './common.css';
import './App.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import {Provider} from 'react-redux';
import store from './store';

import Navbar from './components/Navbar';
import Login from './pages/Login';
import Register from './pages/Register';
import Dashboard from './pages/Dashboard';
import Admin from './pages/Admin';
import ChangePassword from './pages/ChangePassword';
import CompanySelection from './pages/CompanySelection';
import CompanyCreate from './pages/CompanyCreate';
import UserSettings from './pages/UserSettings';
import SendLink from './pages/SendLink';
import Index from './pages/Index';

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Navbar />
        <Switch>
          <Route exact path="/" component={Index}/>
          <Route exact path="/login" component={Login}/>
          <Route exact path='/register' component={Register}/>
          <Route exact path="/dashboard" component={Dashboard} />
          <Route path="/admin" component={Admin} />
          <Route path="/resetpassword" component={ChangePassword}/>
          <Route path="/select" component={CompanySelection} />
          <Route path="/create" component={CompanyCreate} />
          <Route path="/settings" component={UserSettings} />
          <Route path="/sendlink" component={SendLink} />
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;

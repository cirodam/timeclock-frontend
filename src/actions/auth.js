import axios from 'axios';
import { AUTH_LOADING, LOGGED_IN, LOGGED_OUT, LOGIN_ERROR } from './types';
import {API_ROOT} from '../const';

/**
 * Sends a login request to the backend. Then updates the Redux store with the JWT used for authentication.
 * @param  {String} username The username to authenticate with
 * @param  {String} password The password to authenticate with
 */
export const login = (username, password) => async dispatch => {

    dispatch({type: AUTH_LOADING});
    const options = {
        headers: {
            'Content-Type': 'application/json'
        }
    };
    const body = JSON.stringify({username, password});

    try {
        const res = await axios.post(`${API_ROOT}/auth`, body, options);
        dispatch({type: LOGGED_IN, payload: res.data});

    } catch (err) {
        if(err.response){
            dispatch({type: LOGIN_ERROR, payload: err.response.data.error});
        }else{
            dispatch({type: LOGIN_ERROR, payload: "Auth error"});
        }
        
    }
}

/**
 * Removes the JWT from the redux store. Useful for discarding an expired JWT.
 */
export const timeout = () => async dispatch => {
    dispatch({type: LOGGED_OUT, payload: "Timed Out"})
}

/**
 * Removes the JWT from the redux store. The user has chosen to logout.
 */
export const logout = () => async dispatch => {
    dispatch({type: LOGGED_OUT, payload: null});
}

/**
 * Sends a request to the backend to email the user a password reset link.
 * @param  {String} username
 * @param  {email} email the email that the password reset link will be sent to.
 */
export const sendResetLink = (username, email) => async dispatch => {

    const options = {
        headers: {
            'Content-Type': 'application/json'
        }
    };
    const body = JSON.stringify({username, email});

    try {
        const res = await axios.post(`${API_ROOT}/auth/link`, body, options);

    } catch (err) {
        
    }
}
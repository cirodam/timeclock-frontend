import axios from 'axios';
import { 
        COMPANY_CREATED, 
        COMPANY_ERROR, 
        COMPANY_GROUPS_UPDATED, 
        COMPANY_HOLIDAYS_UPDATED, 
        COMPANY_LOADED, 
        COMPANY_LOADING, 
        COMPANY_PROFILES_LOADED,
        COMPANY_UPDATED,
        COMPANY_JOINED } from './types';
import {API_ROOT} from '../const';
import {getOptions} from '../util';

/**
 * Sends a request to the backend to create a new company. Then updates the redux store with the company info.
 * @param  {String} companyName The name for the company
 * @param  {String} companyCode The ID for the company. Users can use this to join a new company.
 */
export const createCompany = (companyName, companyCode) => async dispatch => {

    dispatch({type: COMPANY_LOADING})

    const body = {
        companyName,
        companyCode
    }

    try {
        const res = await axios.post(`${API_ROOT}/companies`, body, getOptions());
        dispatch({type: COMPANY_CREATED, payload: res.data});

    } catch (err) {
        console.log(err);
        dispatch({type: COMPANY_ERROR, payload: err});

    }
}

/**
 * Fetches information about a company from the backend. Updates the redux store with the information.
 * @param  {String} companyID
 */
export const getCompany = (companyID) => async dispatch => {

    dispatch({type: COMPANY_LOADING})

    try {
        const res = await axios.get(`${API_ROOT}/companies/${companyID}`, getOptions());
        dispatch({type: COMPANY_LOADED, payload: res.data});

    } catch (err) {
        console.log(err);
        dispatch({type: COMPANY_ERROR, payload: err});

    }
}

/**
 * Sends a request to the backend to update specified fields of a Company.
 * @param  {String} companyID
 * @param  {Object} updates Specifies the fields to update.
 */
export const updateCompany = (companyID, updates) => async dispatch => {

    dispatch({type: COMPANY_LOADING})

    const body = JSON.stringify(updates);

    try {
        const res = await axios.put(`${API_ROOT}/companies/${companyID}`, body, getOptions());
        dispatch({type: COMPANY_UPDATED, payload: res.data});

    } catch (err) {
        console.log(err);
        dispatch({type: COMPANY_ERROR, payload: err});

    }
}

/**
 * Sends a request to the backend to update a company's holiday list
 * @param  {String} companyID
 * @param  {Array} holidays The list of the updated holidays
 */
export const updateHolidays = (companyID, holidays) => async dispatch => {

    dispatch({type: COMPANY_LOADING})

    const body = JSON.stringify({holidays});

    try {
        const res = await axios.put(`${API_ROOT}/companies/${companyID}/holidays`, body, getOptions());
        dispatch({type: COMPANY_HOLIDAYS_UPDATED, payload: res.data});

    } catch (err) {
        console.log(err);
        dispatch({type: COMPANY_ERROR, payload: err});

    }
}

/**
 * Sends a request to the backend to update a company's group list
 * @param  {String} companyID
 * @param  {String} groups The list of updated groups
 */
export const updateGroups = (companyID, groups) => async dispatch => {

    dispatch({type: COMPANY_LOADING})

    const body = JSON.stringify({groups});

    try {
        const res = await axios.put(`${API_ROOT}/companies/${companyID}/groups`, body, getOptions());
        dispatch({type: COMPANY_GROUPS_UPDATED, payload: res.data});

    } catch (err) {
        console.log(err);
        dispatch({type: COMPANY_ERROR, payload: err});

    }
}

/**
 * Fetches the list of a company's profiles from the backend.
 * @param  {String} companyID
 */
export const getProfiles = (companyID) => async dispatch => {

    dispatch({type: COMPANY_LOADING})

    try {
        const res = await axios.get(`${API_ROOT}/companies/${companyID}/profiles`, getOptions());
        dispatch({type: COMPANY_PROFILES_LOADED, payload: res.data});

    } catch (err) {
        console.log(err);
        dispatch({type: COMPANY_ERROR, payload: err});

    }

}

/**
 * Sends a request to the backend to join a company.
 * @param  {String} companyCode The ID that uniquely identifies the company
 * @param  {String} inviteCode The inviteCode for this company
 */
export const joinCompany = (companyCode, inviteCode) => async (dispatch, getState) => {

    dispatch({type: COMPANY_LOADING})
    const {user} = getState();

    const body = {
        companyCode,
        inviteCode,
        firstName: user.firstName,
        lastName: user.lastName
    }

    try {
        const res = await axios.post(`${API_ROOT}/join`, body, getOptions());
        dispatch({type: COMPANY_JOINED, payload: res.data});

    } catch (err) {
        console.log(err);
        dispatch({type: COMPANY_ERROR, payload: err});

    }

}

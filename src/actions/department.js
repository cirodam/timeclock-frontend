import axios from 'axios';
import {DEPARTMENT_CREATED, DEPARTMENT_HOLIDAYS_UPDATED, DEPARTMENT_LOADED, DEPARTMENT_LOADING, COMPANY_PROFILES_LOADED} from './types';
import {API_ROOT} from '../const';

//Departments are no longer used
//They have been replaced by companys

export const createDepartment = (departmentName) => async dispatch => {

    dispatch({type: DEPARTMENT_LOADING})

    const options = {
        headers: {
            'Content-Type': 'application/json',
            'x-auth-token': localStorage.getItem('token')
        },
        timeout: 10000
    };

    const body = {
        departmentName
    }

    try {
        const res = await axios.post(`${API_ROOT}/departments`, body, options);
        dispatch({type: DEPARTMENT_CREATED, payload: res.data});
    } catch (err) {
        //dispatch({type: LOGIN_FAIL, payload: err.response.data.error});
    }
}

export const getDepartment = (departmentID) => async dispatch => {

    dispatch({type: DEPARTMENT_LOADING})

    const options = {
        headers: {
            'Content-Type': 'application/json',
            'x-auth-token': localStorage.getItem('token')
        }
    };

    try {
        const res = await axios.get(`${API_ROOT}/departments/${departmentID}`, options);
        dispatch({type: DEPARTMENT_LOADED, payload: res.data});
    } catch (err) {
        //dispatch({type: LOGIN_FAIL, payload: err.response.data.error});
    }
}

export const updateDepartment = (departmentID, updates) => async dispatch => {

    dispatch({type: DEPARTMENT_LOADING})

    const options = {
        headers: {
            'Content-Type': 'application/json',
            'x-auth-token': localStorage.getItem('token')
        }
    };
    const body = JSON.stringify(updates);

    try {
        const res = await axios.put(`${API_ROOT}/departments/${departmentID}`, body, options);
        dispatch({type: DEPARTMENT_LOADED, payload: res.data});
    } catch (err) {
        //dispatch({type: LOGIN_FAIL, payload: err.response.data.error});
    }
}

export const updateHolidays = (departmentID, holidays) => async dispatch => {

    dispatch({type: DEPARTMENT_LOADING})

    const options = {
        headers: {
            'Content-Type': 'application/json',
            'x-auth-token': localStorage.getItem('token')
        }
    };
    const body = JSON.stringify({holidays});

    try {
        const res = await axios.put(`${API_ROOT}/departments/${departmentID}/holidays`, body, options);
        dispatch({type: DEPARTMENT_HOLIDAYS_UPDATED, payload: res.data});
    } catch (err) {
        //dispatch({type: LOGIN_FAIL, payload: err.response.data.error});
    }
}

export const getUsers = (departmentID) => async dispatch => {

    dispatch({type: DEPARTMENT_LOADING})

    const options = {
        headers: {
            'Content-Type': 'application/json'
        }
    };

    try {
        const res = await axios.get(`${API_ROOT}/users/${departmentID}`, options);
        dispatch({type: COMPANY_PROFILES_LOADED, payload: res.data});
    } catch (err) {
        //dispatch({type: LOGIN_FAIL, payload: err.response.data.error});
    }

}

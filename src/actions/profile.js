import { PROFILE_DELETED, PROFILE_ERROR, PROFILE_LOADED, PROFILE_LOADING, PROFILE_UPDATED } from "./types";
import {API_ROOT} from '../const';
import axios from 'axios';
import {getOptions} from '../util';

/**
 * Fetches the details of a profile from the backend. Updates the redux store.
 * @param  {String} companyID
 * @param  {String} userID The ID for the particular profile. Corresponds to a user ID.
 */
export const getProfile = (companyID, userID) => async dispatch => {

    dispatch({type: PROFILE_LOADING});

    try {
        const res = await axios.get(`${API_ROOT}/companies/${companyID}/profiles/${userID}`, getOptions());
        dispatch({type: PROFILE_LOADED, payload: res.data});

    } catch (err) {
        console.log(err);
        dispatch({type: PROFILE_ERROR, payload: err});

    }
}

/**
 * Sends a request to the backend to delete a profile from this company. Updates the redux store.
 * @param  {String} companyID
 * @param  {String} userID The ID for the particular profile. Corresponds to a user ID.
 */
export const deleteProfile = (companyID, userID) => async dispatch => {

    dispatch({type: PROFILE_LOADING});

    try {
        const res = await axios.delete(`${API_ROOT}/companies/${companyID}/profiles/${userID}`, getOptions());
        dispatch({type: PROFILE_DELETED, payload: userID});

    } catch (err) {
        console.log(err);
        dispatch({type: PROFILE_ERROR, payload: err});

    }
}

/**
 * Sends a request to the backend to update this profile. Updates the redux store.
 * @param  {String} companyID
 * @param  {String} userID The ID for the particular profile. Corresponds to a user ID.
 * @param  {Object} updates The fields to be updated.
 */
export const updateProfile = (companyID, userID, updates) => async dispatch => {

    dispatch({type: PROFILE_LOADING});
    
    const body = JSON.stringify(updates);

    try {
        const res = await axios.put(`${API_ROOT}/companies/${companyID}/profiles/${userID}`, body, getOptions());
        dispatch({type: PROFILE_UPDATED, payload: res.data});

    } catch (err) {
        console.log(err);
        dispatch({type: PROFILE_ERROR, payload: "Error Updating user"});
    }
}
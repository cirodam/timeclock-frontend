import axios from "axios";
import { TIMESHEET_DAYS_UPDATED, TIMESHEET_ERROR, TIMESHEET_HOURS_CALCULATED, TIMESHEET_LOADED, TIMESHEET_LOADING, TIMESHEET_NOTES_UPDATED, TIMESHEET_PUNCHES_UPDATED } from "./types";
import {API_ROOT} from '../const';
import { getHoursWorked } from "../util";

/**
 * Given the punches loaded in the redux store, calculates the hours worked and updates the store.
 */
export const calculateHours = () => (dispatch, getState) => {

    const {profile, company, timesheet} = getState();

    dispatch({type: TIMESHEET_LOADING});
    const calculated = getHoursWorked(timesheet.punches, timesheet.startClockedIn, +profile.maxOvertimeHours, company.holidays, +profile.maxHolidayHours);

    dispatch({type: TIMESHEET_HOURS_CALCULATED, payload: calculated});
}

/**
 * Fetches the specified timesheet from the backend. Updates the redux store.
 * @param  {String} userID The ID for the particular profile. Corresponds to a user ID.
 * @param  {Number} startTime The timestamp uniquely identifying this timesheet.
 */
export const getTimesheet = (userID, startTime) => async (dispatch, getState) => {

    dispatch({type: TIMESHEET_LOADING});

    const {company} = getState();

    const options = {
        headers: {
            'Content-Type': 'application/json',
            'x-auth-token': localStorage.getItem('token')
        }
    };

    try {
        let {data} = await axios.get(`${API_ROOT}/companies/${company.companyID}/timesheets/${userID}/${startTime}`, options);

        dispatch({type: TIMESHEET_LOADED, payload: data});

    } catch (err) {
        dispatch({type: TIMESHEET_ERROR, payload: err})
    }
}

/**
 * Sends a request to the backend to add a new punch to the specified timesheet. Updates the redux store.
 * @param  {String} userID The ID for the particular profile. Corresponds to a user ID.
 * @param  {Number} startTime The timestamp uniquely identifying this timesheet.
 * @param  {Number} timestamp The timestamp uniquely identifying the new punch
 */
export const addPunch = (userID, startTime, timestamp) => async (dispatch, getState) => {

    dispatch({type: TIMESHEET_LOADING});

    const {user} = getState();

    const options = {
        headers: {
            'Content-Type': 'application/json',
            'x-auth-token': localStorage.getItem('token')
        }
    };

    try {
        const res = await axios.post(`${API_ROOT}/companies/${user.selectedCompany}/timesheets/${userID}/${startTime}/punches?timestamp=${timestamp}`, {}, options);
        dispatch({type: TIMESHEET_PUNCHES_UPDATED, payload: res.data});

    } catch (err) {
        dispatch({type: TIMESHEET_ERROR, payload: err})
    }
}

/**
 * Sends a request to the backend to delete the specified punch of the specified timesheet. Updates the redux store.
 * @param  {String} userID The ID for the particular profile. Corresponds to a user ID.
 * @param  {Number} startTime The timestamp uniquely identifying this timesheet.
 * @param  {Number} timestamp The timestamp uniquely identifying the punch to be deleted.
 */
export const deletePunch = (userID, startTime, timestamp) => async (dispatch, getState) => {

    dispatch({type: TIMESHEET_LOADING});

    const {user} = getState();

    const options = {
        headers: {
            'Content-Type': 'application/json',
            'x-auth-token': localStorage.getItem('token')
        }
    };

    try {
        const res = await axios.delete(`${API_ROOT}/companies/${user.selectedCompany}/timesheets/${userID}/${startTime}/punches?timestamp=${timestamp}`, options);
        dispatch({type: TIMESHEET_PUNCHES_UPDATED, payload: res.data});

    } catch (err) {
        dispatch({type: TIMESHEET_ERROR, payload: err})
    }
}

/**
 * Sends a request to the backend to replace one punch timestamp with another. Updates the redux store.
 * @param  {String} userID The ID for the particular profile. Corresponds to a user ID.
 * @param  {Number} startTime The timestamp uniquely identifying this timesheet.
 * @param  {Number} oldTimestamp The timestamp uniquely identifying the punch to be deleted.
 * @param  {Number} newTimestamp The timestamp uniquely identifying the punch to be added.
 */
export const movePunch = (userID, startTime, oldTimestamp, newTimestamp) => async (dispatch, getState) => {

    dispatch({type: TIMESHEET_LOADING});

    const {user} = getState();

    const options = {
        headers: {
            'Content-Type': 'application/json',
            'x-auth-token': localStorage.getItem('token')
        }
    };

    try {
        const res = await axios.put(`${API_ROOT}/companies/${user.selectedCompany}/timesheets/${userID}/${startTime}/punches?oldTimestamp=${oldTimestamp}&newTimestamp=${newTimestamp}`, {}, options);
        dispatch({type: TIMESHEET_PUNCHES_UPDATED, payload: res.data});

    } catch (err) {
        dispatch({type: TIMESHEET_ERROR, payload: err})
    }
}

/**
 * Sends a request to the backend to update the days array for a given timesheet. Updates the redux store.
 * @param  {String} userID The ID for the particular profile. Corresponds to a user ID.
 * @param  {Number} startTime The timestamp uniquely identifying this timesheet.
 * @param  {Array} days The updated days array.
 */
export const updateDays = (userID, startTime, days) => async (dispatch, getState) => {

    dispatch({type: TIMESHEET_LOADING});
    const {user} = getState();

    const options = {
        headers: {
            'Content-Type': 'application/json',
            'x-auth-token': localStorage.getItem('token')
        }
    };

    const body = {
        days
    }

    try {
        const res = await axios.put(`${API_ROOT}/companies/${user.selectedCompany}/timesheets/${userID}/${startTime}/days`, body, options);
        dispatch({type: TIMESHEET_DAYS_UPDATED, payload: res.data});

    } catch (err) {
        dispatch({type: TIMESHEET_ERROR, payload: err})
    }
}

/**
 * Sends a request to the backend to update the notes string for a given timesheet. Updates the redux store.
 * @param  {String} userID The ID for the particular profile. Corresponds to a user ID.
 * @param  {Number} startTime The timestamp uniquely identifying this timesheet.
 * @param  {String} notes The updated notes string
 */
export const updateNotes = (userID, startTime, notes) => async (dispatch, getState) => {

    dispatch({type: TIMESHEET_LOADING});

    const {user} = getState()

    const options = {
        headers: {
            'Content-Type': 'application/json',
            'x-auth-token': localStorage.getItem('token')
        }
    };

    const body = {
        notes
    }

    try {
        const res = await axios.put(`${API_ROOT}/companies/${user.selectedCompany}/timesheets/${userID}/${startTime}/notes`, body, options);
        dispatch({type: TIMESHEET_NOTES_UPDATED, payload: res.data});

    } catch (err) {
        dispatch({type: TIMESHEET_ERROR, payload: err})
    }
}
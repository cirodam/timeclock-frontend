import { USER_LOADING, USER_CREATED, USER_DELETED, USER_ERROR, USER_LOADED, USER_UPDATED, COMPANY_SELECTED } from "./types";
import axios from 'axios';
import {API_ROOT} from '../const';
import {getOptions} from '../util';

/**
 * Sends a request to the backend to create a new User. Updates the redux store.
 * @param  {Object} user The fields to use for creating the user
 */
export const createUser = user => async dispatch => {

    dispatch({type: USER_LOADING})

    const options = {
        headers: {
            'Content-Type': 'application/json'
        }
    };
    const body = JSON.stringify(user);

    try {
        const res = await axios.post(`${API_ROOT}/users`, body, options);
        dispatch({type: USER_CREATED, payload: res.data});
    } catch (err) {
        console.log(err);
        dispatch({type: USER_ERROR, payload: err});

    }
}

/**
 * Sends a request to the backend to delete a user. Updates the redux store.
 * @param  {String} userID The ID for the user
 */
export const deleteUser = userID => async dispatch => {

    dispatch({type: USER_LOADING})

    try {
        await axios.delete(`${API_ROOT}/users/${userID}`, getOptions());
        dispatch({type: USER_DELETED, payload: {userID}});

    } catch (err) {
        console.log(err);
        dispatch({type: USER_ERROR, payload: err});

    }
}

/**
 * Fetches user data from the backend. Updates the redux store.
 * @param  {String} userID The ID for the user
 */
export const getUser = (userID) => async dispatch => {

    dispatch({type: USER_LOADING});

    try {
        const res = await axios.get(`${API_ROOT}/users/${userID}`, getOptions());
        dispatch({type: USER_LOADED, payload: res.data});

    } catch (err) {
        console.log(err);
        dispatch({type: USER_ERROR, payload: err});

    }
}

/**
 * Sends a request to the backend to update data for a user. Updates the redux store.
 * @param  {String} userID The ID for the user
 * @param  {Object} updates The updated fields for the user
 */
export const updateUser = (userID, updates) => async dispatch => {

    dispatch({type: USER_LOADING});
    
    const body = JSON.stringify(updates);

    try {
        const res = await axios.put(`${API_ROOT}/users/${userID}`, body, getOptions());
        dispatch({type: USER_UPDATED, payload: res.data});

    } catch (err) {
        console.log(err);
        dispatch({type: USER_ERROR, payload: "Error Updating user"});
    }
}

/**
 * Sends a request to the backend to update the password for the current user.
 * @param  {String} password The new password for this user.
 * @param  {String} token The password reset token. Uniquely identifies the user.
 */
export const resetPassword = (password, token) => async dispatch => {

    const options = {
        headers: {
            'Content-Type': 'application/json'
        }
    };
    const body = JSON.stringify({password, token});

    try {
        const res = await axios.post(`${API_ROOT}/auth/resetpassword`, body, options);

    } catch (err) {
        console.log(err);

    }
}

/**
 * Selects the current company for this user. Used for further requests.
 * @param  {String} companyID The ID of the company to select
 */
export const selectCompany = (companyID) => async dispatch => {
    dispatch({type: COMPANY_SELECTED, payload: companyID});
}

import React from 'react';
import {useSelector} from 'react-redux';
import styles from './AdminBar.module.css'
import {Link} from 'react-router-dom';
import { EDIT_COMPANY, GROUP, SELF, VIEW_TIMESHEET } from '../const';

const AdminBar = () => {

    const {permissions} = useSelector(state => state.user);

    if(permissions){
        return (
            <div id={styles.adminBar}>
    
                { permissions[VIEW_TIMESHEET] >= GROUP &&
                    <>
                        <Link to="/dashboard" id={styles.adminBtn}><i className="fas fa-bars"></i></Link>
                        <Link to="/admin/summary" id={styles.adminBtn}><i className="fas fa-chart-line"></i></Link>
                        <Link to="/admin/users" id={styles.adminBtn}><i className="fas fa-user"></i></Link>
                    </>
                }
    
                { permissions[EDIT_COMPANY] >= SELF && 
                    <Link to="/admin/settings" id={styles.adminBtn}><i className="fas fa-cog"></i></Link>
                }
    
            </div>
        )
    }

    return null;
}

export default AdminBar;

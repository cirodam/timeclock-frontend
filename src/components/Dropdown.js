import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import {useHistory} from 'react-router-dom';
import {logout} from '../actions/auth';
import {useDispatch, useSelector} from 'react-redux';
import styles from './Dropdown.module.css';

const Dropdown = () => {

    const history = useHistory();
    const dispatch = useDispatch();

    const [active, setActive] = useState(false);

    const {firstName, lastName, companies, permissions} = useSelector(state => state.user);

    const onDropdownButton = (e) => {
        e.preventDefault();
        setActive(!active);
    }

    const onLogoutButton = () => {
        dispatch(logout());
        history.push('/login')
    }

    return (
        <div className={styles.dropdown}>
            <button onClick={(e) => {onDropdownButton(e)}} className={styles.dropdownButton}>
                <p>{firstName} {lastName} </p>
                {
                    !active ? <i className="fas fa-chevron-down"></i> : <i className="fas fa-chevron-up"></i>
                }
            </button>
            <button onClick={(e) => {onDropdownButton(e)}} className={styles.dropdownButtonMobile}>
                <i className="fas fa-bars"></i>
            </button>
            {
                active &&
                <div className={`${styles.dropdownList} box-shadow`}>
                    {companies && companies.length > 1 && <Link to="/select">Switch Company</Link>}
                    <Link to="/settings">Settings</Link>
                    <button className="btn btn-transparent" onClick={() => onLogoutButton()}>Logout</button>
                </div>
            }
        </div>
    )
}

export default Dropdown;

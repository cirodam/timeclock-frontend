import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {useSelector} from 'react-redux';
import {Link} from 'react-router-dom';
import styles from './GroupEntry.module.css';
import { GROUP, VIEW_TIMESHEET } from '../const';

const GroupEntry = ({group}) => {

    const {profiles} = useSelector(state => state.company);
    const {permissions, group: myGroup} = useSelector(state => state.user);
    const [show, setShow] = useState(true)

    if(!permissions){
        return null;
    }

    if(group === myGroup || permissions[VIEW_TIMESHEET] >= GROUP) {
        return (
            <>
                <li className={group==="Default" ? styles.disabledHeader : styles.groupHeader}>
                    <button className={styles.groupButton} onClick={() => setShow(!show)}>
                        <i className={`fas fa-chevron-${show ? 'up' : 'down'} ${group==="Default" && styles.disabledColor}`}></i>
                    </button>
                    <h2 className={styles.groupName}>{group}</h2>
                </li>
                
                {profiles && show &&
                    profiles.map(p => p.group === group && (
                        <li className={styles.profile}><Link to={`/admin/users/${p.userID}`} key={p.userID}><p className={styles.groupLink}>{p.firstName} {p.lastName}</p></Link></li>
                    ))
                }
            </>
        )
    }

    return null;
}

GroupEntry.propTypes = {
    group: PropTypes.string
}

export default GroupEntry;

import React from 'react';

const Loader = () => {
    return (
        <img src="/Spinning arrow.gif" alt="loading" className="loader-img"/>
    )
}

export default Loader;

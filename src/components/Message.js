import React from 'react';
import styles from './Message.module.css';

const Message = ({children}) => {
    return (
        <div className={styles.message}>
            <p><i className="fas fa-exclamation-circle"></i> {children}</p>
        </div>
    )
}

export default Message;

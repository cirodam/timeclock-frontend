import React from 'react';
import {Link} from 'react-router-dom';
import {useSelector} from 'react-redux';
import Dropdown from '../components/Dropdown';
import styles from './Navbar.module.css';

const Navbar = () => {

    const {isAuthenticated} = useSelector(state => state.auth);

    return (
        <nav id={styles.navbar}>
            <div className={styles.container}>
                <h1 className="text-light">Simple Timeclock</h1>
                {
                    isAuthenticated?
                        <Dropdown /> :
                        <ul>
                            <li><Link to="/login"><span id={styles.navLink}>Login</span></Link></li>
                            <li><Link to="/register"><span id={styles.navLink}>Register</span></Link></li>
                        </ul>
                }
            </div>
        </nav>
    )
}

export default Navbar;

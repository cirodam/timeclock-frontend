import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {updateNotes} from '../actions/timesheet';
import styles from './Notes.module.css';
import { EDIT_TIMESHEET, SELF } from '../const';

const Notes = () => {

    const dispatch = useDispatch();

    const [notes, setNotes] = useState("");

    const {userID, permissions}          = useSelector(state => state.user);
    const {startTime, notes: notesState} = useSelector(state => state.timesheet);

    useEffect(() => {
        setNotes(notesState)
    }, [notesState])

    const onSaveButton = (e) => {
        dispatch(updateNotes(userID, startTime, notes));
    }

    const onTextChange = (e) => {
        setNotes(e.target.value);
    }

    return (
        <>
            <h4 id={styles.notesHeading}>Notes</h4>
            <textarea name="notes" id={styles.notesText} value={notes} onChange={(e) => onTextChange(e)}></textarea>
            { 
                permissions[EDIT_TIMESHEET] >= SELF && (
                    <div id={styles.actionBar}>
                        <button className="btn btn-primary" onClick={(e) => onSaveButton(e)}>Save</button>
                    </div> 
                )
            }
        </>
    )
}

export default Notes;

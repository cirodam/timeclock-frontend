import React, {useState} from 'react';
import {useSelector} from 'react-redux';
import styles from './PrintableUser.module.css';
import {getDates} from '../util';
import {months} from '../const';

const PrintableUser = () => {

    const {firstName, lastName} = useSelector(state => state.user);
    const {notes, days, startTime} = useSelector(state => state.timesheet);

    const [guides, setGuides] = useState(getDates(startTime));

    if(!startTime){
        return null;
    }

    return (
        <div id={styles.printableUser}>
            <h2 className='section-heading'>Timesheet 23/4 - 12/4</h2>
            <h2 className='section-heading'>{firstName} {lastName}</h2>
            <table id={styles.timesheetTable}>
                <thead>
                    <tr>
                        <th></th>
                        <th>Regular</th>
                        <th>Overtime</th>
                        <th>Holiday</th>
                        <th>Vacation</th>
                        <th>Other</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        guides.map(guide => 
                            <tr>
                                <td className={styles.tdStart}>{months[guide.month]} {guide.day}</td>
                                <td className={styles.tdInner}>{days[guide.day] && days[guide.day].regular > 0 ? days[guide.day].regular : ""}</td>
                                <td className={styles.tdInner}>{days[guide.day] && days[guide.day].overtime > 0 ? days[guide.day].overtime : ""}</td>
                                <td className={styles.tdInner}>{days[guide.day] && days[guide.day].holiday > 0 ? days[guide.day].holiday : ""}</td>
                                <td className={styles.tdInner}>{days[guide.day] && days[guide.day].vacation > 0 ? days[guide.day].vacation : ""}</td>
                                <td className={styles.tdInner}>{days[guide.day] && days[guide.day].vacation > 0 ? days[guide.day].vacation : ""}</td>
                            </tr>)
                    }
                </tbody>
            </table>
            <div id={styles.notesArea}>
                <p>Notes: {notes}</p>
            </div>

            <div>
                <div className={styles.inputGroup}>
                    <label htmlFor="">Approved By</label>
                    <input type="text" className="input underline"/>
                </div>
                <div className={styles.inputGroup}>
                    <label htmlFor="">Date</label>
                    <input type="text" className="input underline"/>
                </div>
            </div>
        </div>
    )
}

export default PrintableUser;

import React, {useState} from 'react';
import EditPunchModal from '../modals/EditPunchModal';
import styles from './Punch.module.css';
import PropTypes from 'prop-types';

const Punch = ({timestamp, status, editable}) => {

    const [showEditPunch, setShowEditPunch] = useState(false);
    const date = new Date(timestamp);

    const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    const month = date.getMonth();
    const meridian = date.getHours() >= 12 ? "pm" : "am"
    const hour = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();

    const formated = `${months[month]}  ${date.getDate()} - ${hour}:${date.getMinutes() > 9 ? date.getMinutes() : "0"+date.getMinutes()} ${meridian}`

    const onPunchClick = () => {
        setShowEditPunch(true);
    }

    const onCloseModal = () => {
        setShowEditPunch(false);
        console.log(showEditPunch);
    }

    return (
        <>
            <li className={`${styles.punch} ${editable && 'cursor-point'}`} onClick={() => onPunchClick()}>
                <p>{formated}</p>
                <p>{status}</p>
            </li>
            { editable &&
                <EditPunchModal visible={showEditPunch} onClose={() => onCloseModal()} timestamp={timestamp}/>
            }
        </>
    )
}

Punch.propTypes = {
    timestamp: PropTypes.number,
    status: PropTypes.string,
    editable: PropTypes.bool
}

export default Punch;

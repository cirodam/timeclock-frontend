import React, {useState} from 'react';
import {useSelector} from 'react-redux';
import Punch from '../components/Punch';
import styles from './PunchList.module.css';
import {getClockedInAtIndex} from '../util';
import AddPunchModal from '../modals/AddPunchModal';
import AddShiftModal from '../modals/AddShiftModal';
import { EDIT_TIMESHEET, SELF } from '../const';

const PunchList = () => {

    const [showAddPunch, setShowAddPunch] = useState(false);
    const [showAddShift, setShowAddShift] = useState(false);

    const {permissions}             = useSelector(state => state.user);
    const {userID}                  = useSelector(state => state.profile);
    const {punches, startClockedIn} = useSelector(state => state.timesheet);
    const {admin}                   = useSelector(state => state.auth);

    const onAddPunchButton = () => {
        setShowAddPunch(true);
    }

    const onAddShiftButton = () => {
        setShowAddShift(true);
    }

    if(!punches){
        return null;
    }

    return (
        <>
            <div className={styles.punchList}>
                {
                    punches && punches.length > 0 ? (
                        <ul className={styles.punchListPunches}>
                            { 
                                punches.map((punch,idx) => <Punch key={punch} status={!getClockedInAtIndex(idx, startClockedIn) ? "in" : "out"} timestamp={punch} editable={admin}/>)
                            }
                        </ul> 
                        ) : (
                            <p>No current punches</p>
                        )
                }
            </div>
            {
                permissions[EDIT_TIMESHEET] >= SELF && (
                    <div className="button-bar">
                        <button className="btn btn-primary" onClick={() => onAddPunchButton()}>Add Punch</button>
                        <button className="btn btn-primary" onClick={() => onAddShiftButton()}>Add Shift</button>
                    </div>
                )
            }
            <AddPunchModal userID={userID} visible={showAddPunch} onClose={() => setShowAddPunch(false)}/>
            <AddShiftModal userID={userID} visible={showAddShift} onClose={() => setShowAddShift(false)}/>
        </>
    )
}

export default PunchList;

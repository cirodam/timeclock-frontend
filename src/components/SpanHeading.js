import React from 'react';
import {useSelector} from 'react-redux'
import styles from './SpanHeading.module.css';

const SpanHeading = () => {

    const {latestSpanStart} = useSelector(state => state.company);

    const start = new Date(parseInt(latestSpanStart));
    const end = new Date(parseInt(latestSpanStart));
    
    end.setMonth(start.getMonth());
    end.setDate(start.getDate() + 13);

    if(!latestSpanStart){
        return null
    }

    return <h4 id={styles.heading}>Pay period: {start.getMonth()+1}/{start.getDate()} - {end.getMonth()+1}/{end.getDate()}</h4>
}

export default SpanHeading

import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {useSelector} from 'react-redux';

const SpanSelect = ({onSelect}) => {

    const {firstSpanStart, spanDuration} = useSelector(state => state.company);

    const [spans, setSpans] = useState(null);

    useEffect(() => {

        const getSpans = () => {
            const current = Date.now();
            let latest = firstSpanStart;
            let sets = [];
    
            do {
    
                const start = new Date(latest);
                const end = new Date();
                end.setMonth(start.getMonth());
                end.setDate(start.getDate() + 13);
    
                sets.push({
                    startTime: latest,
                    startMonth: start.getMonth(),
                    startDay: start.getDate(),
                    endMonth: end.getMonth(),
                    endDay: end.getDate() 
                })
    
                latest += spanDuration;
    
            } while(latest < current)
    
            return sets
        }

        if(firstSpanStart !== null){
            setSpans(getSpans());
        }
    }, [firstSpanStart])

    if(!spans) {
        return null;
    }

    const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    const onSelectChange = (e) => {
        onSelect(e.target.value)
    }

    return (
        <select onChange={(e) => onSelectChange(e)} className="select" defaultValue={spans[spans.length-1].startTime}>
            { 
                spans.map((span, idx) => (
                    <option key={idx} value={span.startTime} >
                        {months[span.startMonth] + " " + span.startDay + " - " + months[span.endMonth] + " " + span.endDay}
                    </option>
                ))
            }
        </select>
    )
}

SpanSelect.propTypes = {
    onSelect: PropTypes.func
}

export default SpanSelect;

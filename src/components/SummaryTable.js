import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {useSelector} from 'react-redux'
import {Link} from 'react-router-dom';
import axios from 'axios';
import { API_ROOT, months } from '../const';
import styles from './SummaryTable.module.css';

const SummaryTable = ({startTime}) => {

    const {selectedCompany} = useSelector(state => state.user);

    const getDates = (startTime) => {

        const start = new Date(startTime);
        const dates = [];

        for(let i=0; i<14; i++){
            let next = new Date();
            next.setMonth(start.getMonth());
            next.setDate(start.getDate() + i);
            dates.push({
                month: next.getMonth(),
                day: next.getDate()
            });
        }

        return dates;
    }

    const [report, setReport] = useState(null);
    let current = getDates(parseInt(startTime));

    useEffect(() => {

        const getReport = async (startTime) => {
            const options = {
                headers: {
                    'Content-Type': 'application/json'
                }
            };
            const res = await axios.get(`${API_ROOT}/companies/${selectedCompany}/report?startTime=${startTime}`, options);
            setReport(res.data);
        }

        getReport(startTime)

    }, [startTime, selectedCompany])

    return (
        <table id={styles.summaryTable}>
            <thead>
                <tr>
                    <th className={styles.user}>User</th>
                    {
                        current.map((date, idx) => (
                            <th className="td-minor" key={idx}><span className="month-header">{months[date.month]}</span> {date.day}</th>
                        ))
                    }
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                {   report &&
                    report.map(user => (
                        <tr key={user.userID}>
                            <td className={styles.user}><Link to={`/admin/users/${user.userID}`} className="color-dark">{user.firstName} {user.lastName}</Link></td>
                            {
                                current.map((date, idx) => (
                                    <td className="td-minor" key={idx}>
                                        { user.days && user.days[date.day] && (user.days[date.day].regular + user.days[date.day].overtime + user.days[date.day].holiday !== 0) &&
                                            user.days[date.day].regular + user.days[date.day].overtime + user.days[date.day].holiday
                                        }
                                    </td>
                                ))
                            }
                            <td>{(user.sum/1000/60/60).toFixed(2)}</td>
                        </tr>
                    ))
                }
            </tbody>
        </table>
    )
}

SummaryTable.propTypes = {
    startTime: PropTypes.number
}

export default SummaryTable;

import React, {useEffect, useState} from 'react';
import {useSelector} from 'react-redux';

const Timer = () => {

    const {punches} = useSelector(state => state.timesheet);
    const [seconds, setSeconds] = useState(0);

    useEffect(() => {

        const getSecondsPassed = () => {
            const lastPunch = punches[punches.length-1];
            const diff = Date.now() - lastPunch;
            return (((diff.toFixed(2) / 1000.00) / 60.00) / 60.00).toFixed(2);
        }

        if(punches){
            const interval = setInterval(() => {setSeconds(getSecondsPassed())}, 1000);
            return () => clearInterval(interval);
        }

    }, [punches])

    return <span> ({seconds} hours)</span>
}

export default Timer;

import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {updateDays, calculateHours} from '../actions/timesheet';
import {months} from '../const';
import {getDates} from '../util';
import styles from './Timesheet.module.css';
import {roundDecimal} from '../util';
import {EDIT_TIMESHEET, SELF} from '../const';

const Timesheet = () => {

    const [days, setDays]       = useState(null);
    const [unsaved, setUnsaved] = useState(false);

    const dispatch                                = useDispatch();
    const {permissions}                           = useSelector(state => state.user);
    const {userID}                                = useSelector(state => state.profile);
    const {calculated, days: dayState, startTime} = useSelector(state => state.timesheet);

    useEffect(() => {

        if(dayState){
            setDays(dayState);
        }

        if(calculated){
            let next = dayState;
            for(const day in dayState){
                if(calculated[day]){
                    next[day].regular = roundDecimal(calculated[day].regular/1000/60/60);
                    next[day].overtime = calculated[day].overtime/1000/60/60;
                    next[day].holiday = calculated[day].holiday/1000/60/60;
                    next[day].vacation = calculated[day].vacation/1000/60/60;
                }
            }
            setDays({...next});
        }
    }, [setDays, dayState, calculated])

    if(!startTime || !days) {
        return null;
    }

    let current = getDates(startTime);

    const onTimesheetChange = (day, field, e) => {
        setUnsaved(true);
        setDays({
            ...days,
            [day]: {
                ...days[day],
                [field]: (!isNaN(e.target.value) && e.target.value !== "") ? parseInt(e.target.value) : 0 
            }
        })
    }

    const onSaveButton = (e) => {
        e.preventDefault();
        dispatch(updateDays(userID, startTime, days));
        setUnsaved(false);
    }

    const onFillButton = (e) => {
        setUnsaved(true);
        e.preventDefault();
        dispatch(calculateHours());
    }

    const getCategorySum = category => {
        let sum = 0;
        for(const day in days){
            sum += days[day][category]
        }
        return sum
    }

    return (
        <>
            <table id={styles.timesheetTable}>
                <thead>
                    <tr>
                        <th></th>
                        <th className="td-outer">R<span className={styles.fullHeader}>egular</span></th>
                        <th className="td-outer">O<span className={styles.fullHeader}>vertime</span></th>
                        <th className="td-outer">H<span className={styles.fullHeader}>oliday</span></th>
                        <th className="td-outer">V<span className={styles.fullHeader}>acation</span></th>
                        <th className="td-outer">O<span className={styles.fullHeader}>ther</span></th>
                    </tr>
                </thead>
                <tbody>
                {
                    current.map((date, idx) => (
                        <tr key={idx}>
                            <td className={styles.tdRowStart}><span className={styles.fullHeader}>{months[date.month]}</span> {date.day}</td>
                            <td className={styles.tdInner}><input 
                                type="text" 
                                className={styles.tInput} 
                                value={days[date.day] && days[date.day].regular > 0 ? days[date.day].regular : ""} 
                                onChange={(e) => onTimesheetChange(date.day, "regular", e)}/></td>
                            <td className={styles.tdInner}><input 
                                type="text" 
                                className={styles.tInput} 
                                value={days[date.day] && days[date.day].overtime > 0 ? days[date.day].overtime : ""} 
                                onChange={(e) => onTimesheetChange(date.day, "overtime", e)}/></td>
                            <td className={styles.tdInner}><input 
                                type="text" 
                                className={styles.tInput} 
                                value={days[date.day] && days[date.day].holiday > 0 ? days[date.day].holiday : ""} 
                                onChange={(e) => onTimesheetChange(date.day, "holiday", e)}/></td>
                            <td className={styles.tdInner}><input type="text" className={styles.tInput} /></td>
                            <td className={styles.tdInner}><input type="text" className={styles.tInput} /></td>
                        </tr>
                    ))
                }
                {
                    <tr>
                        <td></td>
                        <td className={styles.tdSum}>{getCategorySum("regular")}</td>
                        <td className={styles.tdSum}>{getCategorySum("overtime")}</td>
                        <td className={styles.tdSum}>{getCategorySum("holiday")}</td>
                        <td className={styles.tdSum}>{getCategorySum("vacation")}</td>
                    </tr>
                }
                </tbody>
            </table>
            {
                permissions[EDIT_TIMESHEET] >= SELF && (
                    <div id={styles.actionBar}>
                        <button className="btn btn-primary" >Fill From Punches &nbsp; <i className="fas fa-chevron-up"></i></button>
                        <button className="btn btn-primary" >Calc Overtime &nbsp; <i className="fas fa-chevron-up"></i></button>
                        <button className="btn btn-primary" onClick={(e) => onSaveButton(e)}>Save</button>
                        <button className="btn btn-primary" onClick={() => window.print()}><i className="fas fa-print"></i></button>  
                    </div> 
                )
            }
        </>
    )
}

export default Timesheet;

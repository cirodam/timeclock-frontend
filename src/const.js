//Global constants

export const API_ROOT = "https://102ezkzp16.execute-api.us-east-1.amazonaws.com/dev";
export const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

export const VIEW_TIMESHEET = "VIEW_TIMESHEET";
export const EDIT_TIMESHEET = "EDIT_TIMESHEET";
export const EDIT_COMPANY = "EDIT_COMPANY";

export const NONE = 0;
export const SELF = 1;
export const GROUP = 2;
export const ALL = 3;
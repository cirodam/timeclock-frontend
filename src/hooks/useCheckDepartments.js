import {useEffect} from 'react';
import {useSelector} from 'react-redux';
import {useHistory} from 'react-router-dom';

const useCheckDepartments = () => {

    const {selectedCompany} = useSelector(state => state.user);
    const history = useHistory();

    useEffect(() => {
        if(!selectedCompany){
            history.push('/select')
        }
    })
}

export default useCheckDepartments;
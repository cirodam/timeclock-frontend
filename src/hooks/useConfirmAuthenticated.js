import {useEffect} from 'react';
import {useHistory} from 'react-router-dom';
import {useSelector} from 'react-redux';

const useConfirmAuthenticated = () => {

    const history = useHistory();
    const {isAuthenticated} = useSelector(state => state.auth);

    useEffect(() => {
        if(!isAuthenticated){
            history.push('/login');
        }
    })
}

export default useConfirmAuthenticated;
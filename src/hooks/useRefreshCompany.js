import {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {getCompany} from '../actions/company';

const useRefreshCompany = () => {

    const dispatch = useDispatch();
    const {selectedCompany} = useSelector(state => state.user);
    const {companyID} = useSelector(state => state.company);

    useEffect(() => {
        if(!companyID || companyID !== selectedCompany){
            dispatch(getCompany(selectedCompany));
        }
    }, [dispatch, companyID, selectedCompany])
}

export default useRefreshCompany;
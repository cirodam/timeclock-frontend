import {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {getProfile} from '../actions/profile';

const useRefreshProfile = userID => {

    const dispatch = useDispatch();
    const {selectedCompany} = useSelector(state => state.user);
    const {userID: currentProfile} = useSelector(state => state.profile);

    useEffect(() => {
        if(!currentProfile || currentProfile !== userID){
            dispatch(getProfile(selectedCompany, userID));
        }
    }, [currentProfile, selectedCompany, dispatch, userID])

}

export default useRefreshProfile;
import {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {getProfiles} from '../actions/company';

const useRefreshRoster = () => {

    const dispatch = useDispatch();
    const {selectedCompany} = useSelector(state => state.user);
    const {profiles} = useSelector(state => state.company);

    useEffect(() => {
        if(!profiles){
            dispatch(getProfiles(selectedCompany));
        }
    }, [profiles, dispatch, selectedCompany])
}

export default useRefreshRoster;
import {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {getTimesheet} from '../actions/timesheet';

const useRefreshTimesheet = (userID, startTime) => {

    const dispatch = useDispatch();
    const {selectedCompany} = useSelector(state => state.user);

    useEffect(() => {

        if(startTime){
            dispatch(getTimesheet(userID, startTime));
        }
    }, [userID, startTime, dispatch, selectedCompany])
}

export default useRefreshTimesheet
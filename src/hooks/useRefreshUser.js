import {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {getUser} from '../actions/user';

const useRefreshUser = userID => {

    const dispatch = useDispatch();
    const {userID: currentUser, username} = useSelector(state => state.user);

    useEffect(() => {
        if(!username || currentUser !== userID){
            dispatch(getUser(userID));
        }
    }, [dispatch, userID, currentUser, username])

}

export default useRefreshUser;
import {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {timeout} from '../actions/auth';

const useTimeoutCheck = () => {

    const dispatch = useDispatch();
    const {expire_time} = useSelector(state => state.auth);

    useEffect(() => {
        if(Date.now() >= expire_time){
            dispatch(timeout());
        }
    })
}

export default useTimeoutCheck;
import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import { addPunch } from '../actions/timesheet';
import {timestampToString} from '../util';
import styles from './AddPunchModal.module.css';
import PropTypes from 'prop-types';

const AddPunchModal = ({userID, visible, onClose}) => {

    const dispatch = useDispatch();

    const {shiftStart} = useSelector(state => state.company);
    const {startTime} = useSelector(state => state.timesheet);
    const [date, setDate] = useState("");
    const [time, setTime] = useState("");
    const [error, setError] = useState(null);

    if(!visible){
        return null;
    }

    const onExitButton = (e) => {
        e.preventDefault();
        setError(null);
        onClose();
    }

    const onAddButton = (e) => {
        e.preventDefault();
        const d = new Date(date + " " + time);

        if(d.getTime() > Date.now()){
            setError("Can't add future punches");
        }else{
            setError(null);
            dispatch(addPunch(userID, startTime, d.getTime()));
            onClose();
        }
    }

    const onTodayButton = (e, offset) => {
        e.preventDefault();

        let t = new Date();
        let tzoffset = (new Date()).getTimezoneOffset() * 60000;
        let d = new Date(t.getTime() - tzoffset);
        d.setDate(d.getDate() + offset)

        setDate(d.toISOString().substring(0,10));
    }

    const onShiftStartButton = (e) => {
        e.preventDefault();
        setTime(timestampToString(shiftStart));
    }

    return (
        <div id={styles.addPunchModal} className="modal">

            <div className="modal-header">
                <h2 className="section-heading">Add Punch</h2>
                <button className="exit-btn" onClick={(e) => onExitButton(e)}><i className="fas fa-times"></i></button>
            </div>

            <form onSubmit={(e) => onAddButton(e)} id={styles.modalForm}>
                <div className={styles.inputLine}>

                    <div className={styles.inputGroup}>
                        <label htmlFor="date" className="input-label">Date</label>
                        <input type="date" name="date" id="date" value={date} onChange={(e) => setDate(e.target.value)} className="input"/>
                        <button className="btn btn-transparent color-dark subtext" onClick={(e) => onTodayButton(e, -1)}>Yesterday</button>{' | '}
                        <button className="btn btn-transparent color-dark subtext" onClick={(e) => onTodayButton(e, 0)}>Today</button>{' | '}
                        <button className="btn btn-transparent color-dark subtext" onClick={(e) => onTodayButton(e, 1)}>Tomorrow</button> 
                    </div>

                    <div className={styles.inputGroup}>
                        <label htmlFor="time" className="input-label">Time</label>
                        <input type="time" name="time" id="time" value={time} onChange={(e) => setTime(e.target.value)} className="input"/>
                        <button className="btn btn-transparent color-dark subtext" onClick={(e) => onShiftStartButton(e)}>Shift Start</button>
                    </div>

                </div>
                { error && <p id={styles.modalError}>{error}</p> }

                <button type="submit" id={styles.inputButton} className="btn btn-primary">Add</button>
            </form>
        </div>
    )
}

AddPunchModal.propTypes = {
    userID: PropTypes.string,
    visible: PropTypes.bool,
    onClose: PropTypes.func
}

export default AddPunchModal;

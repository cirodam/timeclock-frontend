import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import { addPunch } from '../actions/timesheet';
import PropTypes from 'prop-types';

const AddShiftModal = ({userID, visible, onClose}) => {

    const dispatch = useDispatch();

    const {shiftStart, latestSpanStart} = useSelector(state => state.company);
    const [date, setDate] = useState("");
    const [time, setTime] = useState("");

    if(!visible){
        return null;
    }

    const onExitButton = (e) => {
        e.preventDefault();
        onClose();
    }

    const getTimeString = (start) => {
        const minutes = start/1000/60;
        let hours = minutes/60;
        let remain = minutes - (hours*60);

        if(hours <= 9){
            hours = "0" + hours;
        }

        if(remain <= 9){
            remain = "0" + remain;
        }

        return hours + ":" + remain;
    }

    const onSubmit = (e) => {
        e.preventDefault();
        const d = new Date(date + " " + time);

        dispatch(addPunch(userID, latestSpanStart, d.getTime()));
        onClose();
    }

    const onYesterday = (e) => {
        e.preventDefault();

        let t = new Date();
        let tzoffset = (new Date()).getTimezoneOffset() * 60000;
        let d = new Date(t.getTime() - tzoffset);
        d.setDate(d.getDate() - 1)

        setDate(d.toISOString().substring(0,10));
    }

    const onToday = (e) => {
        e.preventDefault();

        let t = new Date();
        let tzoffset = (new Date()).getTimezoneOffset() * 60000;
        let d = new Date(t.getTime() - tzoffset);
        setDate(d.toISOString().substring(0,10));
    }

    const onTomorrow = (e) => {
        e.preventDefault();

        let t = new Date();
        let tzoffset = (new Date()).getTimezoneOffset() * 60000;
        let d = new Date(t.getTime() - tzoffset);
        d.setDate(d.getDate() + 1)

        setDate(d.toISOString().substring(0,10));
    }

    const onShiftStart = (e) => {
        e.preventDefault();

        setTime(getTimeString(shiftStart));
    }

    return (
        <div className="modal">
            <div className="modal-header">
                <h2 className="section-heading">Add Shift</h2>
                <button className="exit-btn" onClick={(e) => onExitButton(e)}><i className="fas fa-times"></i></button>
            </div>
            <form onSubmit={(e) => onSubmit(e)} className="modal-form">
                <div className="input-grid">
                    <div className="input-group">
                        <label htmlFor="date" className="input-label">Start Date</label>
                        <input type="date" name="date" id="date" value={date} onChange={(e) => setDate(e.target.value)} className="input"/>
                        <button className="btn btn-transparent color-dark subtext" onClick={(e) => onYesterday(e)}>Yesterday</button>{' | '}
                        <button className="btn btn-transparent color-dark subtext" onClick={(e) => onToday(e)}>Today</button>{' | '}
                        <button className="btn btn-transparent color-dark subtext" onClick={(e) => onTomorrow(e)}>Tomorrow</button> 
                    </div>
                    <div className="input-group">
                        <label htmlFor="time" className="input-label">Start Time</label>
                        <input type="time" name="time" id="time" value={time} onChange={(e) => setTime(e.target.value)} className="input"/>
                        <button className="btn btn-transparent color-dark subtext" onClick={(e) => onShiftStart(e)}>Shift Start</button>
                    </div>
                    <div className="input-group">
                        <label htmlFor="date" className="input-label">End Date</label>
                        <input type="date" name="date" id="date" value={date} onChange={(e) => setDate(e.target.value)} className="input"/>
                        <button className="btn btn-transparent color-dark subtext" onClick={(e) => onYesterday(e)}>Yesterday</button>{' | '}
                        <button className="btn btn-transparent color-dark subtext" onClick={(e) => onToday(e)}>Today</button>{' | '}
                        <button className="btn btn-transparent color-dark subtext" onClick={(e) => onTomorrow(e)}>Tomorrow</button> 
                    </div>
                    <div className="input-group">
                        <label htmlFor="time" className="input-label">End Time</label>
                        <input type="time" name="time" id="time" value={time} onChange={(e) => setTime(e.target.value)} className="input"/>
                        <button className="btn btn-transparent color-dark subtext" onClick={(e) => onShiftStart(e)}>Shift Start</button>
                    </div>
                </div>
                <button type="submit" className="btn btn-primary">Add</button>
            </form>
        </div>
    )
}

AddShiftModal.propTypes = {
    userID: PropTypes.string,
    visible: PropTypes.bool,
    onClose: PropTypes.func
}

export default AddShiftModal;

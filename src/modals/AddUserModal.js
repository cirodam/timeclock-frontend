import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {createUser} from '../actions/user';
import styles from './AddUserModal.module.css';
import PropTypes from 'prop-types';

const AddUserModal = ({visible, onClose}) => {

    const dispatch = useDispatch();
    const {departmentID} = useSelector(state => state.auth);

    const [formData, setFormData] = useState({
        firstName: "", 
        lastName: "", 
        username: "", 
        email: "", 
        password: "", 
        admin: "",
    })
    const {firstName, lastName, username, email, password, admin} = formData;

    const onInputChange = (e) => {
        setFormData({...formData, [e.target.name]: e.target.value});
    }

    const onExitButton = (e) => {
        e.preventDefault();
        onClose();
    }

    const onAddButton = async (e) => {
        e.preventDefault();
        dispatch(createUser({departmentID, firstName, lastName, username, email, password, admin}));
        onClose();
    }

    if(!visible){
        return null;
    }

    return (
        <div id={styles.addUserModal} className="modal">
            <div className="modal-header">
                <h2 className="section-heading">Add a user</h2>
                <button className="exit-btn" onClick={(e) => onExitButton(e)}><i className="fas fa-times"></i></button>
            </div>
            <form onSubmit={(e) => onAddButton(e)}>
                <div className="input-grid">
                    <div className="input-group">
                        <label htmlFor="firstName" className="input-label">First Name</label>
                        <input type="text" name="firstName" className="input" value={firstName} onChange={e => onInputChange(e)}/>
                    </div>
                    <div className="input-group">
                        <label htmlFor="lastName" className="input-label">Last Name</label>
                        <input type="text" name="lastName" value={lastName} onChange={e => onInputChange(e)} className="input"/>
                    </div>
                    <div className="input-group">
                        <label htmlFor="username" className="input-label">Username</label>
                        <input type="text" name="username" value={username} onChange={e => onInputChange(e)} className="input"/>
                    </div>
                    <div className="input-group">
                        <label htmlFor="email" className="input-label">Email</label>
                        <input type="text" name="email" value={email} onChange={e => onInputChange(e)} className="input"/>
                    </div>
                    <div className="input-group">
                        <label htmlFor="password" className="input-label">Password</label>
                        <input type="text" name="password" value={password} onChange={e => onInputChange(e)} className="input"/>
                    </div>
                    <div className="input-group">
                        <label htmlFor="admin" className="input-label">Administrator</label>
                        <input type="checkbox" name="admin" value={admin} onChange={e => onInputChange(e)} className="input"/>
                    </div>
                </div>
                <button type="submit" className="btn btn-primary ml-30p">Add</button>
            </form>
        </div>
    )
}

AddUserModal.propTypes = {
    visible: PropTypes.bool,
    onClose: PropTypes.func
}

export default AddUserModal;

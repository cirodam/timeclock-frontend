import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {createCompany} from '../actions/company';
import styles from './CreateCompanyModal.module.css';
import PropTypes from 'prop-types';

const CreateCompanyModal = ({visible, onClose}) => {

    const dispatch = useDispatch();
    const {companyID} = useSelector(state => state.auth);

    const [formData, setFormData] = useState({
        departmentName: "", 
        departmentID: ""
    })
    const {departmentName, departmentID} = formData;

    const onInputChange = (e) => {
        setFormData({...formData, [e.target.name]: e.target.value});
    }

    const onExitButton = (e) => {
        e.preventDefault();
        onClose();
    }

    const onAddButton = async (e) => {
        e.preventDefault();
        dispatch(createCompany(departmentName, departmentID));
        onClose();
    }

    if(!visible){
        return null;
    }

    return (
        <div id={styles.createCompanyModal} className="modal">

            <div className="modal-header">
                <h2 className="section-heading">Create an Organization</h2>
                <button className="exit-btn" onClick={(e) => onExitButton(e)} id={styles.exitBtn}><i className="fas fa-times"></i></button>
            </div>

            <form onSubmit={(e) => onAddButton(e)} id={styles.modalForm}>
                <div className={styles.inputGroup}>
                    <label htmlFor="firstName" className="input-label">Organization Name</label>
                    <input type="text" name="departmentName" className="input" value={departmentName} onChange={e => onInputChange(e)}/>
                </div>
                <div className={styles.inputGroup}>
                    <label htmlFor="departmentID" className="input-label">Organization ID</label>
                    <input type="text" name="departmentID" className="input" value={departmentID} onChange={e => onInputChange(e)}/>
                </div>
                <button type="submit" className="btn btn-primary">Create</button>
            </form>

        </div>
    )
}

CreateCompanyModal.propTypes = {
    visible: PropTypes.bool,
    onClose: PropTypes.func
}

export default CreateCompanyModal;

import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import { updateProfile } from '../actions/profile';
import styles from './EditPermissionsModal.module.css';
import PropTypes from 'prop-types';

const EditPermissionsModal = ({visible, onClose}) => {

    const dispatch = useDispatch();
    const {selectedCompany} = useSelector(state => state.user);
    const profile = useSelector(state => state.profile);

    const [permissions, setPermissions] = useState({})

    useEffect(() => {
        if(profile.permissions){
            setPermissions({...profile.permissions})
        }
    }, [profile]);

    if(!visible){
        return null;
    }

    const onExitButton = (e) => {
        e.preventDefault();
        onClose();
    }

    const onSaveButton = (e) => {
        e.preventDefault();

        dispatch(updateProfile(selectedCompany, profile.userID, {permissions}));
        onClose();
    }

    const onSelectChange = e => {
        e.preventDefault();

        setPermissions({...permissions, [e.target.name]: +e.target.value})
    }

    return (
        <div id={styles.EditPermissionsModal} className="modal">
            <div className="modal-header">
                <h2 className="section-heading">Edit Permissions</h2>
                <button className="exit-btn" onClick={(e) => onExitButton(e)}><i className="fas fa-times"></i></button>
            </div>
            <table id={styles.permTable} onSubmit={(e) => onSaveButton(e)}>
                <tbody>

                    <tr>
                        <td>Edit Company</td>
                        <select name="EDIT_COMPANY" value={permissions.EDIT_COMPANY} onChange={(e) => onSelectChange(e)}>
                            <option value="1">Allow</option>
                            <option value="0">Deny</option>
                        </select>
                    </tr>

                    <tr>
                        <td>View Timesheet</td>
                        <select name="VIEW_TIMESHEET" value={permissions.VIEW_TIMESHEET} onChange={(e) => onSelectChange(e)}>
                            <option value="1">Self</option>
                            <option value="2">Group</option>
                            <option value="3">All</option>
                        </select>
                    </tr>

                    <tr>
                        <td>Edit Timesheet</td>
                        <select name="EDIT_TIMESHEET" value={permissions.EDIT_TIMESHEET} onChange={(e) => onSelectChange(e)}>
                            <option value="0">None</option>
                            <option value="1">Self</option>
                            <option value="2">Group</option>
                            <option value="3">All</option>
                        </select>
                    </tr>

                    <tr>
                        <td>Edit Profile</td>
                        <select name="EDIT_PROFILE" value={permissions.EDIT_PROFILE} onChange={(e) => onSelectChange(e)}>
                            <option value="2">Group</option>
                            <option value="3">All</option>
                        </select>
                    </tr>

                </tbody>
            </table>
            <button className="btn btn-primary" type="submit" id={styles.saveButton} onClick={(e) => onSaveButton(e)}>Save</button>
        </div>
    )
}

EditPermissionsModal.propTypes = {
    visible: PropTypes.bool,
    onClose: PropTypes.func
}

export default EditPermissionsModal;

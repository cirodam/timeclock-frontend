import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {deletePunch, movePunch} from '../actions/timesheet';
import styles from './EditPunchModal.module.css';
import PropTypes from 'prop-types';

const EditPunchModal = ({visible, onClose, timestamp}) => {

    const dispatch = useDispatch();
    const {user} = useSelector(state => state.user);
    const {startTime} = useSelector(state => state.timesheet);

    var tzoffset = (new Date()).getTimezoneOffset() * 60000;
    const d = new Date(timestamp - tzoffset)

    const [date, setDate] = useState(d.toISOString().substring(0,10));
    const [time, setTime] = useState(d.toISOString().substring(11,16));
    const [error, setError] = useState(null);

    if(!visible){
        return null;
    }

    const onExitButton = (e) => {
        e.preventDefault();
        setError(null);
        onClose();
    }

    const onDeleteButton = (e) => {
        e.preventDefault();
        dispatch(deletePunch(user.userID, startTime, timestamp));
    }

    const onUpdateButton = (e) => {
        e.preventDefault();

        const t = new Date(date + " " + time);

        if(t.getTime() > Date.now()){
            setError("Can't add future punches");
        } else {
            setError(null);
            dispatch(movePunch(user.userID, startTime, timestamp, t.getTime()))
            onClose();
        }
    }

    return (
        <div id={styles.editPunchModal} className="modal">
            <div className="modal-header">
                <h2 className="section-heading">Edit Punch</h2>
                <button className="exit-btn" onClick={(e) => onExitButton(e)}><i className="fas fa-times"></i></button>
            </div>
            <form id={styles.modalForm} onSubmit={(e) => onUpdateButton(e)}>
                <div className={styles.inputLine}>
                    <div className={styles.inputGroup}>
                        <label htmlFor="date" className="input-label">Date</label>
                        <input type="date" name="date" id="date" value={date} onChange={(e) => setDate(e.target.value)} className="input"/>
                    </div>
                    <div className={styles.inputGroup}>
                        <label htmlFor="time" className="input-label">Time</label>
                        <input type="time" name="time" id="time" value={time} onChange={(e) => setTime(e.target.value)} className="input"/>
                    </div>
                </div>
                { error && <p id={styles.modalError}>{error}</p> }
                <div className={styles.buttonLine}>
                    <button type="submit" id={styles.inputButton} className="btn btn-primary">Update</button>
                    <button type="submit" id={styles.inputButton} className="btn btn-primary" onClick={(e) => onDeleteButton(e)}>Delete</button>
                </div>
            </form>
        </div>
    )
}

EditPunchModal.propTypes = {
    visible: PropTypes.bool,
    onClose: PropTypes.func,
    timestamp: PropTypes.number
}

export default EditPunchModal;

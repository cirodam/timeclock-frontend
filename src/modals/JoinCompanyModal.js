import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {joinCompany} from '../actions/company';
import styles from './JoinCompanyModal.module.css';
import PropTypes from 'prop-types';

const JoinCompanyModal = ({visible, onClose}) => {

    const dispatch = useDispatch();

    const [formData, setFormData] = useState({
        companyCode: "", 
        inviteCode: ""
    })
    const {companyCode, inviteCode} = formData;

    const onInputChange = (e) => {
        setFormData({...formData, [e.target.name]: e.target.value});
    }

    const onExitButton = (e) => {
        e.preventDefault();
        onClose();
    }

    const onAddButton = async (e) => {
        e.preventDefault();
        dispatch(joinCompany(companyCode, +inviteCode));
        onClose();
    }

    if(!visible){
        return null;
    }

    return (
        <div id={styles.JoinCompanyModal} className="modal">
            <div className="modal-header">
                <h2 className="section-heading">Join an Organization</h2>
                <button className="exit-btn" onClick={(e) => onExitButton(e)} id={styles.exitBtn}><i className="fas fa-times"></i></button>
            </div>
            <form onSubmit={(e) => onAddButton(e)} id={styles.modalForm}>
                <div className={styles.inputGroup}>
                    <label htmlFor="companyCode" className="input-label">Organization ID</label>
                    <input type="text" name="companyCode" className="input" value={companyCode} onChange={e => onInputChange(e)}/>
                </div>
                <div className={styles.inputGroup}>
                    <label htmlFor="firstName" className="input-label">Invite Code</label>
                    <input type="text" name="inviteCode" className="input" value={inviteCode} onChange={e => onInputChange(e)}/>
                </div>
                <button type="submit" className="btn btn-primary">Join</button>
            </form>
        </div>
    )
}

JoinCompanyModal.propTypes = {
    visible: PropTypes.bool,
    onClose: PropTypes.func
}

export default JoinCompanyModal;

import React from 'react';
import Summary from '../pages/Summary';
import User from '../pages/User';
import Users from './Users';
import {Link, Route} from 'react-router-dom';
import Settings from './Settings';
import useTimeoutCheck from '../hooks/useTimeoutCheck';
import styles from './Admin.module.css';
import useConfirmAuthenticated from '../hooks/useConfirmAuthenticated';
import AdminBar from '../components/AdminBar';
 
const Admin = () => {

    useTimeoutCheck();
    useConfirmAuthenticated();

    return (
        <div>
            <div className={styles.container}>
                <AdminBar />
                <>
                    <Route exact path="/admin/users" component={Users} />
                    <Route path="/admin/users/:userID" component={User}/>
                    <Route path="/admin/settings" component={Settings} />
                    <Route exact path="/admin/summary" component={Summary} />
                </>
            </div>
        </div>
    )
}

export default Admin;

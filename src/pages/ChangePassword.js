import React, {useState} from 'react'
import {useDispatch} from 'react-redux';
import {useHistory, useLocation} from 'react-router-dom';
import {resetPassword} from '../actions/user';
import styles from './ChangePassword.module.css';

const ChangePassword = () => {

    const dispatch = useDispatch();
    const search = useLocation().search;
    const token = new URLSearchParams(search).get('token');

    const history = useHistory();
    const [formData, setFormData] = useState({
        password: "",
        password2: ""
    })
    const {password, password2} = formData;

    const onSubmitButton = e => {
        e.preventDefault();
        dispatch(resetPassword(password, token));
        history.push('/login');
    }

    const onInputChange = e => {
        setFormData({...formData, [e.target.name]: e.target.value});
    }

    return (
        <div id={styles.changePasswordPage}>
            <form onSubmit={(e) => onSubmitButton(e)} id={styles.changePasswordSection}>
                <h2 id={styles.sectionHeader}>Change your password</h2>
                <div className={styles.inputGroup}>
                    <label htmlFor="password" className="input-label">Password: </label>
                    <input type="password" name="password" className="input" value={password} onChange={(e) => onInputChange(e)}/>
                </div>
                
                <div className={styles.inputGroup}>
                    <label htmlFor="password2" className="input-label">Confirm: </label>
                    <input type="password" name="password2" className="input" value={password2} onChange={(e) => onInputChange(e)}/>
                </div>
                <button type="submit" className="btn btn-primary" id={styles.resetButton}>Update</button>
            </form>
        </div>
    )
}

export default ChangePassword;

import React, {useState, useEffect} from 'react';
import {useSelector} from 'react-redux';
import {useHistory} from 'react-router-dom';
import CreateCompanyModal from '../modals/CreateCompanyModal';
import JoinCompanyModal from '../modals/JoinCompanyModal';
import styles from './CompanyCreate.module.css';

const CompanyCreate = () => {

    const [showCreateDepartment, setShowCreateDepartment] = useState(false);
    const [showJoinCompany, setShowJoinCompany] = useState(false);

    const {companies} = useSelector(state => state.user);
    const history = useHistory();

    useEffect(() => {
        if(companies && companies.length > 0){
            history.push('/select');
        }
    })

    const onCreateButton = () => {
        setShowCreateDepartment(true);
    }

    const onJoinButton = () => {
        setShowJoinCompany(true);
    }

    return (
        <div id={styles.createPage}>
            <div id={styles.createSection} className="box-shadow">
                <h2 id={styles.sectionHeading} className="section-heading">You aren't currently part of an organization</h2>
                <div id={styles.actionButtons}>
                    <button onClick={() => onCreateButton()} className="btn btn-primary">Create an Organization</button>
                    <button onClick={() => onJoinButton()} className="btn btn-primary">Join an Organization</button>
                </div>
            </div>
            <CreateCompanyModal visible={showCreateDepartment} onClose={() => setShowCreateDepartment(false)}/>
            <JoinCompanyModal visible={showJoinCompany} onClose={() => setShowJoinCompany(false)}/>
        </div>
    )

    return null;
}

export default CompanyCreate;

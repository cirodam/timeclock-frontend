import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import { useHistory } from 'react-router';
import {selectCompany} from '../actions/user';

const CompanySelection = () => {

    const {companies, selectedCompany} = useSelector(state => state.user);
    const history = useHistory();
    const dispatch = useDispatch();

    useEffect(() => {
        if(companies){
            if(companies.length === 0){
                history.push('/create');

            }else if(companies.length === 1){
                dispatch(selectCompany(companies[0].companyID));
                history.push('/dashboard');

            }
        }

        if(selectedCompany){
            history.push('/dashboard');
        }
    }, [companies, selectedCompany, history])

    const onCompanyButton = company => {
        dispatch(selectCompany(company.companyID));
        history.push('/dashboard');
    }

    if(companies){
        return (
            <div>
                {
                    companies.map(company => <button onClick={() => onCompanyButton(company)}>{company.companyName}</button>)
                }
            </div>
        )
    }

    return null;
}

export default CompanySelection;

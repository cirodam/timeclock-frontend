import React from 'react';
import {useSelector} from 'react-redux';
import {Redirect} from 'react-router-dom';
import Message from '../components/Message';
import useTimeoutCheck from '../hooks/useTimeoutCheck';
import useRefreshUser from '../hooks/useRefreshUser';
import useRefreshCompany from '../hooks/useRefreshCompany';
import useRefreshTimesheet from '../hooks/useRefreshTimesheet';
import useRefreshProfile from '../hooks/useRefreshProfile';
import DashboardConsoleSection from '../sections/DashboardConsoleSection';
import LoaderSection from '../sections/LoaderSection';
import useConfirmAuthenticated from '../hooks/useConfirmAuthenticated';
import useCheckDepartments from '../hooks/useCheckDepartments';
import ProfileTimesheetSection from '../sections/ProfileTimesheetSection';
import {Link} from 'react-router-dom'

import styles from './Dashboard.module.css';
import AdminBar from '../components/AdminBar';
 
const Dashboard = () => {

    const {userID, selectedCompany} = useSelector(state => state.user);
    const {latestSpanStart} = useSelector(state => state.company);
    const {timesheetError, timesheetLoading} = useSelector(state => state.timesheet);

    useCheckDepartments();
    useRefreshCompany(selectedCompany);
    useRefreshProfile(userID);
    useRefreshUser(userID);
    useRefreshTimesheet(userID, latestSpanStart);
    useTimeoutCheck();
    useConfirmAuthenticated();

    return (
        <div id={styles.dashboard}>
            <div className={styles.container}>

                { timesheetError && <Message message={timesheetError}/> }

                <AdminBar />
                <div id={styles.dashboardContent}>

                    { !timesheetLoading ? 
                        <>
                            <DashboardConsoleSection />
                            <ProfileTimesheetSection />
                        </> 
                        :
                        <LoaderSection />
                    }

                </div>
            </div>
        </div>
    )
}

export default Dashboard;

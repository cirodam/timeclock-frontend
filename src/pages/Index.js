import React, {useEffect} from 'react';
import {useSelector} from 'react-redux';
import {useHistory} from 'react-router-dom';
import useRefreshUser from '../hooks/useRefreshUser';
import styles from './Index.module.css';

const Index = () => {

    const {userID} = useSelector(state => state.user);

    useRefreshUser(userID);


    return (
        <div id={styles.indexPage}>
            <h2 id={styles.indexHeader}>A Simple Solution for Managing Company Time</h2>
        </div>
    )
}

export default Index;

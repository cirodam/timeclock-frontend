import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {login} from '../actions/auth';
import {Link, Redirect} from 'react-router-dom';
import Message from '../components/Message';
import styles from './Login.module.css';

const Login = () => {

    const dispatch = useDispatch();

    const {isAuthenticated, authError} = useSelector(state => state.auth);

    const [formData, setFormData] = useState({
        username: "",
        password: ""
    });
    const {username, password} = formData;

    const onInputChange = e => {
        setFormData({...formData, [e.target.name]: e.target.value});
    }

    const onLoginButton = e => {
        e.preventDefault();
        dispatch(login(username, password));
    }

    if(isAuthenticated){
        return <Redirect to="/select"/>
    }

    return (
        <section className="landing">
            <form id={styles.loginForm} className="bg-light" onSubmit={e => onLoginButton(e)}>
                <h2 className="text-primary">Login</h2>
                <div className={styles.inputGroup}>
                    <label htmlFor="username" className="input-label">Username</label>
                    <input type="text" name="username" value={username} onChange={e => onInputChange(e)} className="input"/>
                </div>
                
                <div className={styles.inputGroup}>
                    <label htmlFor="" className="input-label">Password</label>
                    <input type="password" name="password" value={password} onChange={e => onInputChange(e)} className="input"/>
                </div>
                {
                authError &&
                <Message>{authError}</Message>
                }
                <p id={styles.forgotText}>Forgot Your <Link to="/sendlink"><span id={styles.forgotLink}>Password</span></Link></p>
                <button type="submit" id={styles.loginButton} className="btn btn-primary">Login</button>
            </form>
        </section>
    )
}

export default Login;

import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Redirect} from 'react-router-dom';
import Message from '../components/Message';
import styles from './Register.module.css';
import {createUser} from '../actions/user';

const Register = () => {

    const dispatch = useDispatch();

    const {isAuthenticated, error} = useSelector(state => state.auth);
    const [page, setPage] = useState(1);

    const [formData, setFormData] = useState({
        firstName: "",
        lastName: "",
        username: "",
        email: "",
        password: "",
        password2: "",
    });
    const {firstName, lastName, username, email, password, password2} = formData;

    const onInputChange = e => {
        setFormData({...formData, [e.target.name]: e.target.value});
    }

    const onRegisterButton = e => {
        e.preventDefault();
        dispatch(createUser({
            firstName,
            lastName,
            username,
            email,
            password,
        }))
    }

    if(isAuthenticated){
        return <Redirect to="/dashboard"/>
    }

    return (
        <section className="landing">
            <form id={styles.loginForm} className="bg-light">
                <h2 className="text-primary">Register</h2>

                { page === 1 &&
                    <>
                        <div className={styles.inputGroup}>
                            <label htmlFor="" className="input-label">First Name</label>
                            <input type="text" name="firstName" value={firstName} onChange={e => onInputChange(e)} className="input"/>
                        </div>
                        <div className={styles.inputGroup}>
                            <label htmlFor="" className="input-label">Last Name</label>
                            <input type="text" name="lastName" value={lastName} onChange={e => onInputChange(e)} className="input"/>
                        </div>
                        <div className={styles.inputGroup}>
                            <label htmlFor="" className="input-label">Email</label>
                            <input type="text" name="email" value={email} onChange={e => onInputChange(e)} className="input"/>
                        </div>
                        <div className={styles.inputGroup}>
                            <label htmlFor="username" className="input-label">Username</label>
                            <input type="text" name="username" value={username} onChange={e => onInputChange(e)} className="input"/>
                        </div>
                        <div className={styles.inputGroup}>
                            <label htmlFor="" className="input-label">Password</label>
                            <input type="password" name="password" value={password} onChange={e => onInputChange(e)} className="input"/>
                        </div>
                        <div className={styles.inputGroup}>
                            <label htmlFor="" className="input-label">Confirm Password</label>
                            <input type="password" name="password2" value={password2} onChange={e => onInputChange(e)} className="input"/>
                        </div>
                        <div id={styles.formNavigation}>
                            <button className="btn btn-primary" onClick={(e) => onRegisterButton(e)}>Next</button>
                        </div>
                    </>
                }
                
                {
                    error &&
                    <Message>{error}</Message>
                }
            </form>
        </section>
    )
}

export default Register;

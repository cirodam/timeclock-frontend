import React, {useState} from 'react';
import {useDispatch} from 'react-redux';
import { sendResetLink } from '../actions/auth';
import styles from './SendLink.module.css';

const SendLink = () => {

    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const dispatch = useDispatch();

    const [sent, setSent] = useState(false);

    const onSendButton = e => {
        dispatch(sendResetLink(username, email));
        setSent(true);
    }

    return (
        <div id={styles.sendPage}>
            { !sent ?
                <form id={styles.sendSection} className="box-shadow">
                    <h2 id={styles.sectionHeading} className="section-heading">Forgot Your Password?</h2>
                    <div id={styles.inputGroup} className="input-group">
                        <label className="input-label">Username</label>
                        <input type="text" className="input" value={username} onChange={(e) => setUsername(e.target.value)}/>
                    </div>
                    <div id={styles.inputGroup} className="input-group">
                        <label className="input-label">Email</label>
                        <input type="text" className="input" value={email} onChange={(e) => setEmail(e.target.value)}/>
                    </div>
                    <button id={styles.sendButton} className="btn btn-primary" onClick={(e) => onSendButton(e)}>Send Link</button>
                </form>
                :
                <h2 id={styles.sentHeader}>A password-reset link has been sent to the provided email</h2>
            }


        </div>
    )
}

export default SendLink;

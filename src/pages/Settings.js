import React from 'react';
import {useSelector} from 'react-redux';
import CompanyGroupsSection from '../sections/CompanyGroupsSection';
import CompanyHolidaysSection from '../sections/CompanyHolidaysSection';
import CompanySettingsSection from '../sections/CompanySettingsSection';
import useRefreshCompany from '../hooks/useRefreshCompany';
import useRefreshUser from '../hooks/useRefreshUser';
import LoaderSection from '../sections/LoaderSection';
import styles from './Settings.module.css';

const Settings = () => {

    const {companyLoading} = useSelector(state => state.company);
    const {userID, selectedCompany} = useSelector(state => state.user);

    useRefreshUser(userID);
    useRefreshCompany(selectedCompany);
    
    return (
        <div id={styles.settings}>
            <div className={styles.container}>
                {   companyLoading ?
                        <LoaderSection />
                    :
                    <>
                        <section id={styles.settingsTitle} className="section">
                            <h1 className="page-heading">Settings</h1>
                        </section>
                        <CompanySettingsSection />
                        <CompanyGroupsSection />
                        <CompanyHolidaysSection />
                    </>
                }
            </div>
        </div>
    )
}

export default Settings;

import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import SummaryTable from '../components/SummaryTable';
import SpanSelect from '../components/SpanSelect';
import styles from './Summary.module.css';
import useRefreshUser from '../hooks/useRefreshUser';
import useRefreshCompany from '../hooks/useRefreshCompany';

const Summary = () => {
 
    const {userID, selectedCompany} = useSelector(state => state.user);
    const {latestSpanStart} = useSelector(state => state.company)
    const [setTime, setSetTime] = useState();

    useRefreshUser(userID);
    useRefreshCompany(selectedCompany);

    useEffect(() => {
        if(latestSpanStart){
            setSetTime(latestSpanStart)
        }
    }, [latestSpanStart])

    const onSpanSelect = (timestamp) => {
        setSetTime(timestamp)
    }

    if(!latestSpanStart){
        return null;
    }

    return (
        <div id={styles.summary}>
            <section id={styles.summaryByDay} className="box-shadow">
                <h1 className="section-heading">Summary</h1>
                <SpanSelect onSelect={onSpanSelect}/>
                { setTime &&
                    <SummaryTable startTime={setTime} />
                }
            </section>
        </div>
    )
}

export default Summary;

import React from 'react';
import {useSelector} from 'react-redux';
import ProfileSettingsSection from '../sections/ProfileSettingsSection';
import LoaderSection from '../sections/LoaderSection';
import Message from '../components/Message';
import useTimeoutCheck from '../hooks/useTimeoutCheck';
import useRefreshTimesheet from '../hooks/useRefreshTimesheet';
import useRefreshUser from '../hooks/useRefreshUser';
import UserSidebarSection from '../sections/UserSidebarSection';
import ProfileTimesheetSection from '../sections/ProfileTimesheetSection';
import ProfileNameSection from '../sections/ProfileNameSection';
import PrintableUser from '../components/PrintableUser';
import useRefreshCompany from '../hooks/useRefreshCompany';
import styles from './User.module.css'
import useConfirmAuthenticated from '../hooks/useConfirmAuthenticated';
import useRefreshProfile from '../hooks/useRefreshProfile';
import useRefreshRoster from '../hooks/useRefreshRoster';

const User = ({match}) => {

    const {latestSpanStart} = useSelector(state => state.company);
    const {userID, selectedCompany} = useSelector(state => state.user);
    const {profileLoading, profileError} = useSelector(state => state.profile);
    
    useRefreshUser(userID);
    useRefreshCompany(selectedCompany);
    useRefreshRoster();
    useRefreshProfile(match.params.userID);
    useTimeoutCheck();
    useRefreshTimesheet(match.params.userID, latestSpanStart);
    useConfirmAuthenticated();

    console.log("Drawing User");

    return (
        <div id={styles.user}>
            <UserSidebarSection />
            <div id={styles.userContent}>

                { profileError && <Message>{profileError}</Message> }

                { !profileLoading 
                    ?
                        <>                    
                            <ProfileNameSection />
                            <ProfileTimesheetSection />
                            <ProfileSettingsSection />
                            <PrintableUser/>
                        </> 
                    :
                        <LoaderSection />
                }

            </div>
        </div>
    )
}

export default User;

import React from 'react';
import {useSelector} from 'react-redux';
import UserSettingsSection from '../sections/UserSettingsSection';
import styles from './UserSettings.module.css';
import useRefreshUser from '../hooks/useRefreshUser';

const UserSettings = () => {

    const {userID} = useSelector(state => state.user); 
    useRefreshUser(userID);

    return (
        <div id={styles.settingsPage}>
            <div id={styles.container}>
                <UserSettingsSection />
            </div>
        </div>
    )
}

export default UserSettings;

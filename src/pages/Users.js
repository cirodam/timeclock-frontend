import React, {useState} from 'react';
import { Link } from 'react-router-dom';
import { useSelector} from 'react-redux';
import AddUserModal from '../modals/AddUserModal';
import useTimeoutCheck from '../hooks/useTimeoutCheck';
import useRefreshRoster from '../hooks/useRefreshRoster';
import useRefreshUser from '../hooks/useRefreshUser';
import useRefreshCompany from '../hooks/useRefreshCompany';
import styles from './/Users.module.css';
import useConfirmAuthenticated from '../hooks/useConfirmAuthenticated';
import GroupEntry from '../components/GroupEntry';

const Users = () => {

    const {groups} = useSelector(state => state.company);
    const {userID, selectedCompany} = useSelector(state => state.user);

    const [showAddModal, setShowAddModal] = useState(false);

    useRefreshUser(userID);
    useRefreshCompany(selectedCompany);
    useTimeoutCheck();
    useRefreshRoster();
    useConfirmAuthenticated();

    return (
        <div id={styles.users}>
            <div id={styles.usersSection} className="box-shadow">
                <h2 className="section-heading">Groups</h2>
                <ul id={styles.groupList}>
                    { groups &&
                        groups.map(g => g !== "Default" && <GroupEntry group={g}/>)
                    }
                    <GroupEntry group={"Default"}/>
                </ul>
            </div>
        </div>
    )
}

export default Users;

import { LOGGED_IN, LOGGED_OUT, LOGIN_ERROR } from '../actions/types';

const initialState = {
    token: localStorage.getItem('token'),
    expire_time: localStorage.getItem('expire'),
    isAuthenticated: (localStorage.getItem('token') ? true : false),
    authLoading: false,
    authError: null
}

export default function reducer(state = initialState, {type, payload}){

    switch (type) {
        case LOGGED_IN:
            localStorage.setItem('token', payload.token);
            localStorage.setItem('expire', payload.expire_time);
            return {
                token: payload.token,
                expire_time: payload.expire_time,
                isAuthenticated: true,
                authLoading: false,
                authError: null
            }

        case LOGGED_OUT:
            localStorage.removeItem('token');
            localStorage.removeItem('expire');
            return {
                token: null,
                expire_time: null,
                isAuthenticated: false,
                authLoading: false,
                authError: null
            }

        case LOGIN_ERROR:
            return {
                ...state,
                authLoading: false,
                authError: payload
            }

        default:
            return state;
    }
}
import reducer from './auth';
import {LOGGED_IN} from '../actions/types';

test('Test LOGGED_IN with a valid token', () => {

    const previousState = {
        token: localStorage.getItem('token'),
        expire_time: localStorage.getItem('expire'),
        isAuthenticated: (localStorage.getItem('token') ? true : false),
        authLoading: false,
        authError: null
    }

    let input = {
        type: LOGGED_IN, 
        payload: {
            token: "1234", 
            expire_time: 1235
        }
    }

    let expected = {
        token: "1234",
        expire_time: 1235,
        authError: null,
        authLoading: false,
        isAuthenticated: true
    }

    expect(reducer(previousState, input)).toEqual(expected);
})
import { COMPANY_CREATED, COMPANY_ERROR, COMPANY_GROUPS_UPDATED, COMPANY_HOLIDAYS_UPDATED, COMPANY_LOADED, COMPANY_LOADING, COMPANY_PROFILES_LOADED, COMPANY_UPDATED, LOGGED_OUT, PROFILE_DELETED } from '../actions/types';

const initialState = {
    profiles: null,
    companyID: null,
    companyName: null,
    companyCode: null,
    latestSpanStart: null,
    firstSpanStart: null,
    spanDuration: null,
    shiftStart: null,
    shiftDuration: null,
    holidays: null,
    inviteCode: null,
    groups: null,
    companyError: null,
    companyLoading: null,
}

export default function reducer(state = initialState, {type, payload}){

    switch (type) {
        case COMPANY_LOADING:
            return {
                ...state,
                companyLoading: true
            }

        case COMPANY_CREATED:
            let {company} = payload;
            return {
                ...state, 
                companyID: company.companyID,
                companyName: company.companyName,
                companyCode: company.companyCode,
                latestSpanStart: company.latestSpanStart,
                firstSpanStart: company.firstSpanStart,
                spanDuration: company.spanDuration,
                shiftStart: company.shiftStart,
                shiftDuration: company.shiftDuration,
                holidays: company.holidays,
                inviteCode: company.inviteCode,
                groups: company.groups,
                companyLoading: false
            }

        case COMPANY_LOADED:
        case COMPANY_UPDATED:
            return {
                ...state, 
                companyID: payload.companyID,
                companyName: payload.companyName,
                companyCode: payload.companyCode,
                latestSpanStart: payload.latestSpanStart,
                firstSpanStart: payload.firstSpanStart,
                spanDuration: payload.spanDuration,
                shiftStart: payload.shiftStart,
                shiftDuration: payload.shiftDuration,
                holidays: payload.holidays,
                inviteCode: payload.inviteCode,
                groups: payload.groups,
                companyLoading: false
            }

        case COMPANY_HOLIDAYS_UPDATED:
            return {
                ...state,
                holidays: payload,
                companyLoading: false
            }
        
        case COMPANY_GROUPS_UPDATED:
            return {
                ...state,
                groups: payload,
                companyLoading: false   
            }

        case COMPANY_PROFILES_LOADED:
            return {
                ...state,
                profiles: payload,
                companyLoading: false
            }

        case PROFILE_DELETED:
            return {
                ...state,
                profiles: state.profiles.filter(prof => prof.userID !== payload),
                companyLoading: false
            }

        case COMPANY_ERROR:
            return {
                ...state,
                companyLoading: false,
                companyError: payload
            }

        case LOGGED_OUT:
            return {...initialState}

        default:
            return state;
    }
}
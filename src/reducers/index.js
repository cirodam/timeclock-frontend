import {combineReducers} from 'redux';
import auth from './auth';
import company from './company';
import user from './user';
import timesheet from './timesheet';
import profile from './profile';

export default combineReducers({
    auth, 
    company,
    user,
    timesheet,
    profile
})
import { PROFILE_ERROR, PROFILE_LOADED, PROFILE_LOADING, PROFILE_UPDATED, LOGGED_OUT, PROFILE_DELETED } from '../actions/types';

const initialState = {
    userID: null,
    firstName: null,
    lastName: null, 
    group: null,
    permissions: null,
    maxOvertimeHours: null, 
    maxHolidayHours: null,
    profileLoading: false,
    profileError: null
}

export default function reducer(state = initialState, {type, payload}){

    switch (type) {
        case PROFILE_LOADING:
            return {
                ...state,
                profileLoading: true,
                profileError: null,
            }
        case PROFILE_LOADED: 
            return {
                userID: payload.userID,
                firstName: payload.firstName,
                lastName: payload.lastName,
                group: payload.group,
                permissions: payload.permissions,
                maxHolidayHours: payload.maxHolidayHours,
                maxOvertimeHours: payload.maxOvertimeHours,
                profileLoading: false,
                profileError: null,
            }
        case PROFILE_UPDATED:
            return {
                userID: payload.userID,
                firstName: payload.firstName,
                lastName: payload.lastName,
                group: payload.group,
                permissions: payload.permissions,
                maxHolidayHours: payload.maxHolidayHours,
                maxOvertimeHours: payload.maxOvertimeHours,
                profileLoading: false,
                profileError: null,
            }
        case PROFILE_ERROR:
            return {
                ...state,
                profileLoading: false,
                profileError: payload,
            }
        case PROFILE_DELETED:
        case LOGGED_OUT:
            return {...initialState}
        default:
            return state;
    }
}
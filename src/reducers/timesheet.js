import { TIMESHEET_ERROR, TIMESHEET_LOADED, TIMESHEET_PUNCHES_UPDATED, TIMESHEET_LOADING, TIMESHEET_DAYS_UPDATED, TIMESHEET_HOURS_CALCULATED, TIMESHEET_NOTES_UPDATED, LOGGED_OUT } from '../actions/types';

const initialState = {
    userID: null,
    startTime: null,
    punches: null,
    days: null,
    approvedBy: null,
    approvedTime: null,
    notes: null,
    calculated: null,
    startClockedIn: false,
    timesheetLoading: true,
    timesheetError: null
}

export default function reducer(state = initialState, {type, payload}){

    switch (type) {
        case TIMESHEET_LOADING:
            return {
                ...state,
                timesheetLoading: true,
                timesheetError: null
            }
        case TIMESHEET_LOADED: 
            return {
                userID: payload.userID,
                startTime: payload.startTime,
                punches: payload.punches,
                days: payload.days,
                approvedBy: payload.approvedBy,
                approvedTime: payload.approvedTime,
                notes: payload.notes,
                calculated: payload.calculated,
                startClockedIn: payload.startClockedIn,
                timesheetLoading: false,
                timesheetError: null
            }
        case TIMESHEET_PUNCHES_UPDATED: 
            return {
                ...state,
                punches: payload,
                timesheetLoading: false
            }
        case TIMESHEET_DAYS_UPDATED:
            return {
                ...state,
                days: payload,
                timesheetLoading: false
            }
        case TIMESHEET_ERROR:
            return {
                ...state,
                timesheetError: payload,
                timesheetLoading: false
            }
        case TIMESHEET_HOURS_CALCULATED:
            return {
                ...state,
                calculated: payload,
                timesheetLoading: false
            }
        case TIMESHEET_NOTES_UPDATED:
            return {
                ...state,
                notes: payload,
                timesheetLoading: false
            }
        case LOGGED_OUT:
            return {
                userID: null,
                startTime: null,
                punches: null,
                days: null,
                approvedBy: null,
                approvedTime: null,
                notes: null,
                calculated: null,
                startClockedIn: false,
                timesheetLoading: true,
                timesheetError: null
            }
        default:
            return state;
    }
}
import { USER_LOADING, USER_LOADED, USER_ERROR, USER_UPDATED, LOGGED_OUT, COMPANY_SELECTED, LOGGED_IN, PROFILE_LOADED, COMPANY_CREATED, COMPANY_JOINED } from '../actions/types';

const initialState = {
    userID: localStorage.getItem('userID'),
    firstName: null,
    lastName: null, 
    email: null,
    username: null,
    companies: null,
    selectedCompany: localStorage.getItem('selectedCompany'),
    permissions: null,
    userLoading: false,
    userError: null,
    group: null
}

export default function reducer(state = initialState, {type, payload}){

    switch (type) {
        case USER_LOADING:
            return {
                ...state,
                userLoading: true,
                userError: null,
            }

        case USER_LOADED: 
            localStorage.setItem('userID', payload.userID);
            return {
                ...state,
                userID: payload.userID,
                firstName: payload.firstName,
                lastName: payload.lastName,
                email: payload.email,
                username: payload.username,
                companies: payload.companies,
                userLoading: false,
                userError: null
            }

        case USER_UPDATED:
            localStorage.setItem('userID', payload.userID);
            return {
                ...state,
                userID: payload.userID,
                firstName: payload.firstName,
                lastName: payload.lastName,
                email: payload.email,
                username: payload.username,
                companies: payload.companies,
                userLoading: false,
                userError: null
            }

        case LOGGED_IN:
            localStorage.setItem('userID', payload.user.userID);
            return {
                ...state,
                userID: payload.user.userID,
                firstName: payload.user.firstName,
                lastName: payload.user.lastName,
                email: payload.user.email,
                username: payload.user.username,
                companies: payload.user.companies
            }

        case USER_ERROR:
            return {
                ...state,
                userLoading: false,
                userError: payload
            }

        case COMPANY_CREATED:
            return {
                ...state,
                companies: payload.user
            }

        case COMPANY_JOINED:
            return {
                ...state,
                companies: payload.company
            }

        case COMPANY_SELECTED:
            localStorage.setItem('selectedCompany', payload);
            return {
                ...state,
                selectedCompany: payload
            }

        case LOGGED_OUT:
            localStorage.removeItem('userID');
            localStorage.removeItem('selectedCompany');
            return {...initialState}

        case PROFILE_LOADED:
            if(payload.userID === state.userID){
                return {
                    ...state,
                    permissions: payload.permissions,
                    group: payload.group
                }
            }
            return state;

        default:
            return state;
    }
}
import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux'
import {updateGroups} from '../actions/company';
import styles from './CompanyGroupsSection.module.css';

const CompanyGroupsSection = () => {

    const dispatch = useDispatch();

    const [groups, setGroups] = useState([]);

    const company = useSelector(state => state.company);

    useEffect(() => {
        if(company.companyName){
            setGroups(company.groups);
        }
    }, [company])

    const onAddGroupButton = () => {
        setGroups([...groups, "New_Group"])
    }

    const onChangeGroupButton = (index, e) => {

        setGroups(groups.map((hol, idx) => (idx === index) ? e.target.value : hol))
    }

    const onDeleteButton = (index) => {

        setGroups(groups.filter((hol, idx) => (idx !== index)));
    }

    const onSaveButton = () => {
        dispatch(updateGroups(company.companyID, groups))
    }

    return (
        <section id={styles.settingsGroups} className="section">
            <h2 className="section-heading">Groups</h2>

            <ul id={styles.groupList}>
                {
                    groups.map((group, idx) => (
                        <li key={idx} className={styles.groupItem}>
                            <input type="text" placeholder="name" className={styles.groupNameInput} value={group} onChange={(e) => onChangeGroupButton(idx, e)}/>
                            <button onClick={() => onDeleteButton(idx)} className={styles.removeGroupBtn}><i className="fas fa-times"></i></button>
                        </li>
                    ))
                }
                <li><button className={styles.addGroupBtn} onClick={() => onAddGroupButton()}>Add &nbsp;<i className="fas fa-plus"></i></button></li>
            </ul>

            <div id={styles.controlButtons}>
                <button className="btn btn-primary" onClick={() => onSaveButton()}>Save</button>
            </div>

        </section>
    )
}

export default CompanyGroupsSection;

import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux'
import {updateHolidays} from '../actions/company';
import styles from './CompanyHolidaysSection.module.css';

const CompanyHolidaysSection = () => {

    const dispatch = useDispatch();
    const [holidays, setHolidays] = useState([]);
    const company = useSelector(state => state.company);

    useEffect(() => {
        if(company.companyName){
            setHolidays(company.holidays);
        }
    }, [company])

    const onAddHolidayButton = () => {
        setHolidays([...holidays, {name: "", month: 1, day: 1}])
    }

    const onChangeHolidayButton = (index, field, e) => {

        if(field === "name"){
            setHolidays(holidays.map((hol, idx) => (idx === index) ? {...hol, [field]: e.target.value} : hol))
        }else if(!isNaN(e.target.value)){
            if(e.target.value === ""){
                setHolidays(holidays.map((hol, idx) => (idx === index) ? {...hol, [field]: 0} : hol))
            } else {
                setHolidays(holidays.map((hol, idx) => (idx === index) ? {...hol, [field]: parseInt(e.target.value)} : hol))
            }
        }
    }

    const onDeleteButton = (index) => {

        setHolidays(holidays.filter((hol, idx) => (idx !== index)));
    }

    const onUpButton = (index) => {

        let next = [...holidays]
        next.splice(index, 1);
        next.splice(index-1, 0, holidays[index]);

        setHolidays([...next]);
    }

    const onSaveButton = () => {
        dispatch(updateHolidays(company.companyID, holidays))
    }

    return (
        <section id={styles.settingsHolidays} className="section">
            <h2 className="section-heading">Holidays</h2>

            <ul id={styles.holidayList}>
                {
                    holidays.map((holiday, idx) => (
                        <li key={idx} className={styles.holidayItem}>
                            <input type="text" placeholder="name" className={styles.holidayNameInput} value={holiday.name} onChange={(e) => onChangeHolidayButton(idx, "name", e)}/>
                            <select className={styles.holidayMonthSelect} onChange={(e) => onChangeHolidayButton(idx, "month", e)} value={holiday.month}>
                                <option value={1}>Jan</option>
                                <option value={2}>Feb</option>
                                <option value={3}>Mar</option>
                                <option value={4}>Apr</option>
                                <option value={5}>May</option>
                                <option value={6}>Jun</option>
                                <option value={7}>Jul</option>
                                <option value={8}>Aug</option>
                                <option value={9}>Sep</option>
                                <option value={10}>Oct</option>
                                <option value={11}>Nov</option>
                                <option value={12}>Dec</option>
                            </select>
                            <input type="text" placeholder="day" className={styles.holidayDayInput} value={holiday.day} onChange={(e) => onChangeHolidayButton(idx, "day", e)}/>
                            <button onClick={() => onDeleteButton(idx)} className={styles.holidayModBtn}><i className="fas fa-times"></i></button>
                            { idx > 0 && <button onClick={() => onUpButton(idx)} className={styles.holidayModBtn}><i className="fas fa-chevron-up"></i></button> }
                        </li>
                    ))
                }
                <li><button className={styles.addButton} onClick={() => onAddHolidayButton()}>Add &nbsp;<i className="fas fa-plus"></i></button></li>
            </ul>

            <div id={styles.controlButtons}>
                <button className="btn btn-primary" onClick={() => onSaveButton()}>Save</button>
            </div>
        </section>
    )
}

export default CompanyHolidaysSection;

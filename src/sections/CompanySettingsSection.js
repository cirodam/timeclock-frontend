import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {updateDepartment} from '../actions/department';
import styles from './CompanySettingsSection.module.css';
import {stringToTimestamp, timestampToString} from '../util';
import { updateCompany } from '../actions/company';

const CompanySettingsSection = () => {

    const dispatch = useDispatch();
    const company = useSelector(state => state.company);
    const {selectedCompany} = useSelector(state => state.user);

    const [formData, setFormData] = useState({
        companyName: "",
        companyCode: "",
        spanDuration: "",
        firstSpanStart: "",
        shiftDuration: "",
        shiftStart: "",
        maxUsers: 0,
        inviteCode: 0
    });
    const {companyName, companyCode, spanDuration, firstSpanStart, shiftDuration, shiftStart, maxUsers, inviteCode} = formData;

    useEffect(() => {
        if(company.companyName){
            setFormData({
                companyName: company.companyName || "",
                companyCode: company.companyCode || "",
                spanDuration: company.spanDuration / 1000 / 60 / 60 / 24,
                firstSpanStart: (new Date(company.firstSpanStart)).toISOString().substring(0,10),
                shiftDuration: company.shiftDuration/1000/60/60,
                shiftStart: timestampToString(company.shiftStart),
                maxUsers: company.maxUsers || 0,
                inviteCode: company.inviteCode || 0
            })
        }
    }, [company])

    const onInputChange = e => {
        setFormData({...formData, [e.target.name]: e.target.value});
    }

    const onCheckboxToggle = e => {
        setFormData({...formData, [e.target.name]:!e.target.value});
    }

    const hasBeenChanged = (original, current) => {
        return (current !== "" && current !== original) ? current : undefined;
    }

    const onSaveButton = e => {
        e.preventDefault();

        let tzoffset = (new Date()).getTimezoneOffset() * 60000;

        let updates = {
            companyName,
            companyCode,
            spanDuration: (spanDuration*24*60*60*1000),
            firstSpanStart: (new Date(firstSpanStart)).getTime() + tzoffset,
            shiftStart: stringToTimestamp(shiftStart),
            shiftDuration: (shiftDuration*60*60*1000),
            maxUsers
        }

        updates[companyName] = hasBeenChanged(company.companyName, companyName);
        updates[companyCode] = hasBeenChanged(company.companyCode, companyCode);
        updates[spanDuration] = hasBeenChanged(company.spanDuration / 1000 / 60 / 60 / 24, spanDuration)*24*60*60*1000;

        dispatch(updateCompany(selectedCompany, updates))
    }

    const onRedoBtn = e => {
        e.preventDefault();
        let num = Math.floor(Math.random() * (999999 - 100000) + 100000);
        setFormData({...formData, inviteCode:num});
    }

    return (
        <section id={styles.settingsDepartment} className="section">
            <h2 className="section-heading">Company Settings</h2>
            <form onSubmit={(e) => onSaveButton(e)}>
                <div id={styles.departmentForm}>
                    <div className="input-group">
                        <label htmlFor="departmentName" className="input-label">Company Name</label>
                        <input type="text" name="companyName" value={companyName} onChange={e => onInputChange(e)} className="input"/>
                    </div>
                    <div className="input-group">
                        <label htmlFor="companyCode" className="input-label">Company Code</label>
                        <input type="text" name="companyCode" value={companyCode} onChange={e => onInputChange(e)} className="input"/>
                    </div>
                    <div className="input-group">
                        <label htmlFor="spanDuration" className="input-label">Pay Period Duration</label>
                        <input type="text" name="spanDuration" value={spanDuration} onChange={e => onInputChange(e)} className="input"/>
                    </div>
                    <div className="input-group">
                        <label htmlFor="periodStart" className="input-label">Pay Period Start</label>
                        <input type="Date" name="firstSpanStart" value={firstSpanStart} onChange={e => onInputChange(e)} className="input"/>
                    </div>
                    <div className="input-group">
                        <label htmlFor="shiftDuration" className="input-label">Default Shift Duration</label>
                        <input type="text" name="shiftDuration" value={shiftDuration} onChange={e => onInputChange(e)} className="input"/>
                    </div>
                    <div className="input-group">
                        <label htmlFor="shiftStart" className="input-label">Default Shift Start</label>
                        <input type="time" name="shiftStart" value={shiftStart} onChange={e => onInputChange(e)} className="input"/>
                    </div>
                    <div className="input-group">
                        <label htmlFor="minOvertimeHours" className="input-label">Maximum Users</label>
                        <input type="text" name="maxUsers" value={maxUsers} onChange={e => onInputChange(e)} className="input"/>
                    </div>
                    <div className="input-group">
                        <label htmlFor="minOvertimeHours" className="input-label">Invite Code</label>
                        <div id={styles.inviteInput}>
                            <input type="text" name="maxUsers" value={inviteCode} className="input"/>
                            <button id={styles.redoBtn} onClick={(e) => onRedoBtn(e)}><i className="fas fa-redo"></i></button>
                        </div>
                    </div>
                </div>
                <div id={styles.actionBar}>
                    <button type="submit" className="btn btn-primary">Save</button>
                </div>
            </form>
        </section>
    )
}

export default CompanySettingsSection;

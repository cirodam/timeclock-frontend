import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import Loader from '../components/Loader';
import Timer from '../components/Timer';
import {addPunch} from '../actions/timesheet';
import styles from './DashboardConsoleSection.module.css';
import {getClockedIn} from '../util';

const DashboardConsoleSection = () => {

    const dispatch = useDispatch();
    const {userID} = useSelector(state => state.user);
    const {latestSpanStart, companyName} = useSelector(state => state.company);
    const {timesheetLoading, punches, startClockedIn} = useSelector(state => state.timesheet);

    const onAddPunchButton = () => {
        dispatch(addPunch(userID, latestSpanStart, Date.now()));
    }

    return (
        <section id={styles.dashboardConsole} className="section">
        {
            timesheetLoading ? <Loader /> :
            !getClockedIn(punches, startClockedIn) ?
                <>
                    <h2 id={styles.companyHeader}>{companyName}</h2>
                    <h4 id={styles.clockHeader}>You Are Clocked-Out</h4>
                    <button className = "btn btn-success" onClick={() => onAddPunchButton()} id={styles.clockBtn}>Clock in</button>
                </> :
                <>
                    <h2 id={styles.companyHeader}>{companyName}</h2>
                    <h4 id={styles.clockHeader}>You Are Clocked-In<Timer /></h4>
                    <button className="btn btn-danger" onClick={() => onAddPunchButton()} id={styles.clockBtn}>Clock out</button>
                </>
        }
        </section>
    )
}

export default DashboardConsoleSection;

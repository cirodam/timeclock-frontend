import React, { useState } from 'react';
import {useSelector} from 'react-redux';
import SpanHeading from '../components/SpanHeading';
import PunchList from '../components/PunchList';
import styles from './DashboardPunchesSection.module.css';
import Timesheet from '../components/Timesheet';

const DashboardPunchesSection = () => {

    const [timesheetMode, setTimesheetMode] = useState(true)

    const {admin, userID} = useSelector(state => state.auth);
    const {usersCanEdit} = useSelector(state => state.company);

    const onTimesheetBtn = e => {
        if(!timesheetMode){
            setTimesheetMode(true);
        }
    }

    const onPunchesBtn = e => {
        if(timesheetMode){
            setTimesheetMode(false);
        }
    }

    return (
        <section id={styles.dashboardPunches} className="section">
            <SpanHeading/>
            <div id={styles.controlBtns}>
                <button className={`${styles.controlBtn} ${timesheetMode && styles.selected}`} onClick={(e) => onTimesheetBtn(e)}>Timesheet</button>
                <button className={`${styles.controlBtn} ${!timesheetMode && styles.selected}`} onClick={(e) => onPunchesBtn(e)}>Punches</button>
            </div>
            {
                !timesheetMode ?
                    <>
                        <PunchList />
                    </>
                :
                    <Timesheet/>
            }
        </section>
    )
}

export default DashboardPunchesSection;

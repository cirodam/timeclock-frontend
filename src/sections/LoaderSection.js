import React from 'react';
import styles from './LoaderSection.module.css';

const LoaderSection = () => {
    return (
        <section className={styles.loaderSection}>
            <h2>Loading</h2>
            <img src="/Spinning arrow.gif" alt="loading"/>
        </section>
    )
}

export default LoaderSection;

import React from 'react';
import {useSelector} from 'react-redux';
import styles from './ProfileNameSection.module.css';

const UserNameSection = () => {

    const {firstName, lastName} = useSelector(state => state.profile);

    if(!firstName || !lastName) {
        return null;
    }

    console.log("Drawing name");
    
    return (
        <section id={styles.username} className="section">
            <h2 className="page-heading">{firstName} {lastName}</h2>
        </section>
    )
}

export default UserNameSection;

import React, {useState, useEffect} from 'react'
import {useSelector, useDispatch} from 'react-redux';
import {updateNotes} from '../actions/timesheet';
import styles from './ProfileNotesSection.module.css';

const ProfileNotesSection = () => {
    
    const dispatch = useDispatch();
    const [notes, setNotes] = useState("");
    const [unsaved, setUnsaved] = useState(false);
    const {userID} = useSelector(state => state.user);
    const {startTime, notes: notesState} = useSelector(state => state.timesheet);

    useEffect(() => {
        setNotes(notesState)
    }, [notesState])

    const onChange = (e) => {
        setNotes(e.target.value);
        setUnsaved(true);
    }

    const onSubmitButton = (e) => {
        dispatch(updateNotes(userID, startTime, notes));
        setUnsaved(false);
    }

    if(!startTime){
        return null;
    }

    console.log("Drawing Notes");

    return (
        <section id={styles.userNotes} className="section">
            <h2 className="section-heading">Notes</h2>
            <textarea name="notes" id={styles.timesheetNotes} value={notes} onChange={(e) => onChange(e)}></textarea>
            <div className={styles.controlButtons}>
                { unsaved && 
                    <button className="btn btn-primary" onClick={(e) => onSubmitButton(e)}>Save</button>
                }
            </div>
        </section>
    )
}

export default ProfileNotesSection;

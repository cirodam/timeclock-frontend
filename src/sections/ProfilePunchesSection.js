import React, {useState} from 'react';
import {useSelector} from 'react-redux';
import PunchList from '../components/PunchList';
import AddPunchModal from '../modals/AddPunchModal';
import styles from './ProfilePunchesSection.module.css'

const ProfilePunchesSection = () => {

    const [showAddPunch, setShowAddPunch] = useState(false);
    const {userID} = useSelector(state => state.user);

    console.log("Drawing Punches");

    return (
        <section id={styles.userPunches} className="section">
            <h2 className="section-heading">Punches</h2>
            <PunchList />
            <div className="button-bar">
                <button className="btn btn-primary" onClick={() => setShowAddPunch(true)}>Add Punch</button>
            </div>
            <AddPunchModal userID={userID} visible={showAddPunch} onClose={() => setShowAddPunch(false)}/>
        </section>
    )
}

export default ProfilePunchesSection;

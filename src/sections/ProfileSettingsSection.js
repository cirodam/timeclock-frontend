import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {useHistory} from 'react-router-dom';
import {deleteUser} from '../actions/user';
import {updateProfile} from '../actions/profile';
import styles from './ProfileSettingsSection.module.css';
import EditPermissionsModal from '../modals/EditPermissionsModal';

const ProfileSettingsSection = () => {

    const dispatch = useDispatch();
    const history = useHistory();
    const {selectedCompany} = useSelector(state => state.user);
    const profile = useSelector(state => state.profile);
    const {groups} = useSelector(state => state.company);

    const [showEditPermissions, setShowEditPermissions] = useState(false);

    const [formData, setFormData] = useState({
        group: "",
        maxOvertimeHours: 0,
        maxHolidayHours: 0
    })
    const {group, maxHolidayHours, maxOvertimeHours} = formData;

    useEffect(() => {

        setFormData({
            group: profile.group || "",
            maxOvertimeHours: profile.maxOvertimeHours || 0,
            maxHolidayHours: profile.maxHolidayHours || 0
        })

    }, [profile])

    const onDelete = (e) => {
        e.preventDefault();
        dispatch(deleteUser(profile.departmentID, profile.userID))
        history.push('/admin/users/')
    }

    const onUpdate = (e) => {
        e.preventDefault();

        let update = {};

        update.group            = (group !== "" && group !== profile.group) ? group : undefined
        update.maxHolidayHours  = (maxHolidayHours !== "" && maxHolidayHours !== profile.maxHolidayHours) ? maxHolidayHours : undefined
        update.maxOvertimeHours = (maxOvertimeHours !== "" && maxOvertimeHours !== profile.maxOvertimeHours) ? maxOvertimeHours : undefined

        dispatch(updateProfile(selectedCompany, profile.userID, update));
    }

    const onChange = e => {
        setFormData({...formData, [e.target.name]: e.target.value});
    }

    if(profile.firstname === null || groups === null){
        return null;
    }

    return (
        <section id={styles.userSettings} className="section">
            <h2 className="section-heading">User Settings</h2>
            <form>
                <div id={styles.userEditForm}>
                    <div className={styles.inputGroup}>
                        <label htmlFor="firstName" className="input-label">Permissions</label>
                        <button onClick={(e) => {e.preventDefault(); setShowEditPermissions(true)}} className="btn btn-primary">Edit</button>
                    </div>
                    <div className={styles.inputGroup}>
                        <label htmlFor="lastName" className="input-label">Group</label>
                        <select className="select" name="group" onChange={(e) => onChange(e)}>
                            {
                                groups.map(g => <option value={g} selected={g == group ? true : false}>{g}</option>)
                            }
                        </select>
                    </div>
                    <div className={styles.inputGroup}>
                        <label htmlFor="username" className="input-label">Max Overtime Hours</label>
                        <input type="text" id="username" name="maxOvertimeHours" value={maxOvertimeHours} onChange={e => onChange(e)} className="input"/>
                    </div>
                    <div className={styles.inputGroup}>
                        <label htmlFor="email" className="input-label">Max Holiday Hours</label>
                        <input type="text" id="email" name="maxHolidayHours" value={maxHolidayHours} onChange={e => onChange(e)} className="input"/>
                    </div>
                </div>
                <div id={styles.actionBar}>
                    <button type="submit" className="btn btn-primary" onClick={(e) => onUpdate(e)}>Save</button>
                    <button onClick={(e) => onDelete(e)} className="btn btn-primary">Remove</button>
                </div>
            </form>
            <EditPermissionsModal visible={showEditPermissions} onClose={() => setShowEditPermissions(false)}/>
        </section>
    )
}

export default ProfileSettingsSection;

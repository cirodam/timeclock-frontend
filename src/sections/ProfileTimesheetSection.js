import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import SpanSelect from '../components/SpanSelect';
import Timesheet from '../components/Timesheet';
import {getTimesheet, updateDays} from '../actions/timesheet';
import styles from './ProfileTimesheetSection.module.css'
import PunchList from '../components/PunchList';
import {updateNotes} from '../actions/timesheet'
import Punch from '../components/Punch';
import Notes from '../components/Notes';

const ProfileTimesheetSection = () => {

    const dispatch = useDispatch()
    const {userID} = useSelector(state => state.user);
    const [mode, setMode] = useState(0);
    const [notes, setNotes] = useState("");
    const {startTime, notes: notesState} = useSelector(state => state.timesheet);

    const onSpanSelect = (timestamp) => {
        dispatch(getTimesheet(userID, timestamp))
    }

    const onTimesheetBtn = e => {
        if(mode !== 0){
            setMode(0);
        }
    }

    const onPunchesBtn = e => {
        if(mode !== 1){
            setMode(1);
        }
    }

    const onNotesBtn = e => {
        if(mode !== 2){
            setMode(2);
        }
    }

    const onSaveNotes = (e) => {
        dispatch(updateNotes(userID, startTime, notes));
    }

    return (
        <section id={styles.timesheetSection} className="section">

            <div id={styles.headingRow}>
                <SpanSelect onSelect={onSpanSelect}/>
                <button className={`${styles.controlBtn} ${mode===0 && styles.selected}`} onClick={(e) => onTimesheetBtn(e)}>Timesheet</button>
                <button className={`${styles.controlBtn} ${mode===1 && styles.selected}`} onClick={(e) => onPunchesBtn(e)}>Punches</button>
                <button className={`${styles.controlBtn} ${mode===2 && styles.selected}`} onClick={(e) => onNotesBtn(e)}>Notes</button>
            </div>

            { mode === 0 &&
                <>
                    <Timesheet />
                </>
            }

            {
                mode === 1 && 
                <PunchList />
            }

            {
                mode === 2 && 
                <Notes />
            }

        </section>
    )
}

export default ProfileTimesheetSection;

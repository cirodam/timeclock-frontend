import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {useHistory} from 'react-router-dom';
import {updateUser} from '../actions/user';
import styles from './UserSettingsSection.module.css';

const UserSettingsSection = () => {

    const dispatch = useDispatch();
    const history = useHistory();
    const user = useSelector(state => state.user);

    const [formData, setFormData] = useState({
        firstName: "",
        lastName: "",
        email: "",
        username: ""
    })
    const {firstName, lastName, email, username} = formData;

    useEffect(() => {

        if(user) {
            setFormData({
                firstName: user.firstName || "",
                lastName: user.lastName || "",
                email: user.email || "",
                username: user.username || ""
            })
        }

    }, [user])

    const onDelete = (e) => {
        e.preventDefault();
        history.push('/register')
    }

    const onUpdate = (e) => {
        e.preventDefault();

        let update = {};

        update.firstName = (firstName !== "" && firstName !== user.firstName) ? firstName : undefined
        update.lastName  = (lastName !== "" && lastName !== user.lastName) ? lastName : undefined
        update.email     = (email !== "" && email !== user.email) ? email : undefined
        update.username  = (username !== "" && username !== user.username) ? username : undefined

        dispatch(updateUser(user.userID, update));
    }

    const onChange = e => {
        setFormData({...formData, [e.target.name]: e.target.value});
    }

    return (
        <section id={styles.userSettings} className="section">
            <h2 className="section-heading">User Settings</h2>
            <form>
                <div id={styles.userEditForm}>
                    <div className="input-group">
                        <label htmlFor="firstName" className="input-label">First Name</label>
                        <input type="text" id="firstName" name="firstName" value={firstName} onChange={e => onChange(e)} className="input" />
                    </div>
                    <div className="input-group">
                        <label htmlFor="lastName" className="input-label">Last Name</label>
                        <input type="text" id="lastName" name="lastName" value={lastName} onChange={e => onChange(e)} className="input"/>
                    </div>
                    <div className="input-group">
                        <label htmlFor="username" className="input-label">Email</label>
                        <input type="text" id="username" name="email" value={email} onChange={e => onChange(e)} className="input"/>
                    </div>
                    <div className="input-group">
                        <label htmlFor="email" className="input-label">Username</label>
                        <input type="text" id="email" name="username" value={username} onChange={e => onChange(e)} className="input"/>
                    </div>
                </div>
                <button type="submit" className="btn btn-primary" onClick={(e) => onUpdate(e)}>Save</button>
                <button onClick={(e) => onDelete(e)} className="btn btn-primary">Close Account</button>
            </form>
        </section>
    )
}

export default UserSettingsSection;

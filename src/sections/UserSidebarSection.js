import React, {useState} from 'react';
import {useSelector} from 'react-redux';
import AddUserModal from '../modals/AddUserModal';
import {Link} from 'react-router-dom';
import useRefreshRoster from '../hooks/useRefreshRoster';
import styles from './UserSidebarSection.module.css';
import GroupEntry from '../components/GroupEntry';
 
const UserSidebarSection = () => {

    const [showAddModal, setShowAddModal] = useState(false);

    const {groups, companyLoading} = useSelector(state => state.company);

    useRefreshRoster();

    return (
        <div id={styles.userSidebar} className="box-shadow">
            <h2 id={styles.sidebarHeader}>Groups</h2>
            <ul id={styles.groupList}>
                { !companyLoading && groups ?
                    groups.map(g => g !== "Default" && (
                        <GroupEntry group={g}/>
                    )) :
                    null
                }
                <GroupEntry group={"Default"}/>
            </ul>
        </div>
    )
}

export default UserSidebarSection;


//Gets the headers for use when fetching from the backend
export const getOptions = () => {
    return {
        headers: {
            'Content-Type': 'application/json',
            'x-auth-token': localStorage.getItem('token')
        }
    };
}

//Calculates the hours worked by an employee when given their punches
export const getHoursWorked = (punches, startClockedIn, minOvertime, holidays, maxHolidayPay) => {

    console.log(minOvertime)

    let chunks = [];

    if(punches.length < 2){
        return [];
    }

    if(startClockedIn){
        punches.shift();
    }

    for(let i=0; i<punches.length; i += 2){
        if(punches[i+1]){
            chunks.push({
                start: punches[i],
                duration: (punches[i+1] - punches[i])
            })
        }
    }

    let days = {};
    let currentDay = new Date(chunks[0].start);
    let time = 0;
    let cumulative = 0;

    for(let e=0; e<chunks.length; e++){

        let chunkDay = new Date(chunks[e].start);

        if(chunkDay.getDate() === currentDay.getDate()){
            time += chunks[e].duration;
        }else{
            days[currentDay.getDate()] = getDay(currentDay, cumulative, time, minOvertime, holidays, maxHolidayPay);
            cumulative += days[currentDay.getDate()].regular;
            currentDay = new Date(chunks[e].start)
            time = chunks[e].duration
        }

        if(e === chunks.length-1){
            days[currentDay.getDate()] = getDay(currentDay, cumulative, time, minOvertime, holidays, maxHolidayPay);
        }
    }

    return days;
}

//Used by getHoursWorked to calculate the hours worked for a given day
const getDay = (currentDay, cumulative, time, minOvertime, holidays, maxHolidayPay) => {

    let holiday = 0;
    for(const hol in holidays){
        if(holidays[hol].day === currentDay.getDate() && holidays[hol].month === (currentDay.getMonth()+1)){
            holiday = (time > maxHolidayPay) ? maxHolidayPay : time;
        }
    }

    if(cumulative + time > minOvertime) {
        let overshot = (cumulative + time) - minOvertime;
        return {regular: (time-overshot).toFixed(2), overtime: overshot.toFixed(2), holiday: holiday.toFixed(2), vacation: 0}
    }

    return {
        regular: time.toFixed(2),
        overtime: 0,
        holiday: holiday.toFixed(2),
        vacation: 0
    }
}

//Given a starting timestamp, returns the next 14 dates
export const getDates = (startTime) => {

    const start = new Date(parseInt(startTime));
    const dates = [];

    for(let i=0; i<14; i++){
        let next = new Date();
        next.setMonth(start.getMonth());
        next.setDate(start.getDate() + i);
        dates.push({
            month: next.getMonth(),
            day: next.getDate()
        });
    }

    return dates;
}

//Returns a timestamp converted to a string
export const timestampToString = (start) => {
    const minutes = start/1000/60;
    let hours = minutes/60;
    let remain = minutes - (hours*60);

    if(hours <= 9){
        hours = "0" + hours;
    }

    if(remain <= 9){
        remain = "0" + remain;
    }

    return hours + ":" + remain;
}

//Converts a string to a timestamp
export const stringToTimestamp = (start) => {

    const s = start.split(':');
    console.log(s);

    return (s[0]*60*60 + s[1]*60) * 1000
}

//Is this user currently clocked in
export const getClockedIn = (punches, startClockedIn) => {

    if(punches.length % 2 === 0){
        return startClockedIn
    }

    return !startClockedIn;
}

//After the user performed the punch at index, did they become clocked in or clocked out 
export const getClockedInAtIndex = (index, startClockedIn) => {
    
    if(index % 2 === 0){
        return startClockedIn
    }

    return !startClockedIn;
}

//Round this decimal to 2 places
export const roundDecimal = target => {
    return Math.round((target + Number.EPSILON) * 100) / 100;
}
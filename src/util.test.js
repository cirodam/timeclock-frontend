import {getClockedIn} from './util';

describe("Test getClockedIn", () => {

    test('test with 1 punch and not startClockedIn', () => {
        expect(getClockedIn([123456], false)).toBe(true);
    })

    test('test with 2 punches and not startClockedIn', () => {
        expect(getClockedIn([123456, 123456], false)).toBe(false);
    })

    test('test with 2 punches and not startClockedIn', () => {
        expect(getClockedIn([123456], true)).toBe(false);
    })

    test('test with 2 punches and not startClockedIn', () => {
        expect(getClockedIn([123456, 123456], true)).toBe(true);
    })
})